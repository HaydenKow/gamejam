/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\include\scene\scene.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\include\scene
 * Created Date: Wednesday, January 22nd 2020, 11:01:18 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef SCENE_H
#define SCENE_H

#include "common.h"

typedef struct scene
{
    void (*init)(void);
    void (*exit)(void);
    void (*render2D)(float);
    void (*render3D)(float);
    void (*update)(float);
    int flags;
} scene;

#define STARTUP_SCENE(_init, _exit, _render2, _render3, _update, _flags) struct scene scene_start = {_init, _exit, _render2, _render3, _update, _flags}
#define SCENE(_local, _init, _exit, _render2, _render3, _update, _flags) struct scene _local = {_init, _exit, _render2, _render3, _update, _flags}

#define SCENE_FALLTHROUGH_RENDER 1
#define SCENE_FALLTHROUGH_UPDATE 2
#define SCENE_BLOCK 4
#define SCENE_NO_REINIT_CHILD 8

scene *SCN_ChangeTo(scene next);
scene *SCN_Push(scene next);
scene *SCN_Pop(void);
scene *SCN_Current(void);
scene *SCN_Peek(void);

#endif /* SCENE_H */
