#ifndef SWEEP_H
#define SWEEP_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\voxel_physics\sweep.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\voxel_physics
 * Created Date: Wednesday, July 10th 2019, 8:25:33 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include "common.h"
#include <cglm/cglm.h>
#include "voxel_physics.h"

float sweep(blockTest getVoxel, void* _chunk, AABB *box, vec3 dir, CollisionCallback callback, bool noTranslate);

#endif /* SWEEP_H */
