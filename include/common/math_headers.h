/*
 * File: math.h
 * Project: common
 * File Created: Sunday, 31st May 2020 9:44:56 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

#include <math.h>

#ifdef PSP
#include <pspmath.h>
#define SIN(x) vfpu_sinf(x)
#define COS(x) vfpu_cosf(x)
#define SQRT(x) sqrtf(x)
#endif
#ifdef _arch_dreamcast
#include <dc/fmath.h>
#define SIN(x) fsin(x)
#define COS(x) fcos(x)
#define SQRT(x) fsqrt(x)
#endif
#if defined(WINDOWS) || defined(__linux)
#define SIN(x) sinf(x)
#define COS(x) cosf(x)
#define SQRT(x) sqrtf(x)
#endif

#include <cglm/cglm.h>