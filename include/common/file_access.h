#ifndef FILE_ACCESS_H
#define FILE_ACCESS_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\file_access.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, July 6th 2019, 2:08:24 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "common.h"

#if defined(_arch_dreamcast) || (defined(PSP) && defined(PSP_ISO))
const char *transform_path(const char *path);
#else
#define transform_path(path) (path)
#endif

int Sys_FileLength(FILE *f);
bool Sys_FileExists(const char *path);

#endif /* FILE_ACCESS_H */
