/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\obj_loader.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Friday, August 2nd 2019, 6:56:14 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#pragma once
#include "common.h"

typedef struct model_obj {
  float *tris; /* pos 3f, uv 2f, color 3f */
  float min[3];
  float max[3];
  int32_t num_tris;
  int32_t num_faces;
  uint32_t texture;
  uint32_t crc;
  int32_t is_ptr;
} model_obj;

model_obj *OBJ_load(const char *path);
model_obj *OBJ_load_boolean(const char *path, bool transform);
void OBJ_destroy(model_obj **obj);
void OBJ_bind(model_obj *obj);
