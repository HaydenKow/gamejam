#ifndef CUBE_DATA_H
#define CUBE_DATA_H
unsigned char _box_indices[] = {0, 1, 2, 2, 3, 0,         // front
                                4, 5, 6, 6, 7, 4,         // right
                                8, 9, 10, 10, 11, 8,      // top
                                12, 13, 14, 14, 15, 12,   // left
                                16, 17, 18, 18, 19, 16,   // bottom
                                20, 21, 22, 22, 23, 20};  // back

float _box_vertices3[] = {1, 1, 1, 1, 1, 1,  // v0 (front)
                          0, 1, 1, 1, 1, 0,  // v1
                          0, 0, 1, 1, 0, 0,  // v2
                          1, 0, 1, 1, 0, 1,  // v3

                          1, 1, 1, 1, 1, 1,  // v0 (right)
                          1, 0, 1, 1, 0, 1,  // v3
                          1, 0, 0, 0, 0, 1,  // v4
                          1, 1, 0, 0, 1, 1,  // v5

                          1, 1, 1, 1, 1, 1,  // v0 (top)
                          1, 1, 0, 0, 1, 1,  // v5
                          0, 1, 0, 0, 1, 0,  // v6
                          0, 1, 1, 1, 1, 0,  // v1

                          0, 1, 1, 1, 1, 0,  // v1 (left)
                          0, 1, 0, 0, 1, 0,  // v6
                          0, 0, 0, 0, 0, 0,  // v7
                          0, 0, 1, 1, 0, 0,  // v2

                          0, 0, 0, 0, 0, 0,  // v7 (bottom)
                          1, 0, 0, 0, 0, 1,  // v4
                          1, 0, 1, 1, 0, 1,  // v3
                          0, 0, 1, 1, 0, 0,  // v2

                          1, 0, 0, 0, 0, 1,   // v4 (back)
                          0, 0, 0, 0, 0, 0,   // v7
                          0, 1, 0, 0, 1, 0,   // v6
                          1, 1, 0, 0, 1, 1};  // v5

float _box_vertices_center3[] =
    {0.5f, 0.5f, 0.5f, 1, 1, 1,    // v0 (front)
     -0.5f, 0.5f, 0.5f, 1, 1, 0,   // v1
     -0.5f, -0.5f, 0.5f, 1, 0, 0,  // v2
     0.5f, -0.5f, 0.5f, 1, 0, 1,   // v3

     0.5f, 0.5f, 0.5f, 1, 1, 1,    // v0 (right)
     0.5f, -0.5f, 0.5f, 1, 0, 1,   // v3
     0.5f, -0.5f, -0.5f, 0, 0, 1,  // v4
     0.5f, 0.5f, -0.5f, 0, 1, 1,   // v5

     0.5f, 0.5f, 0.5f, 1, 1, 1,    // v0 (top)
     0.5f, 0.5f, -0.5f, 0, 1, 1,   // v5
     -0.5f, 0.5f, -0.5f, 0, 1, 0,  // v6
     -0.5f, 0.5f, 0.5f, 1, 1, 0,   // v1

     -0.5f, 0.5f, 0.5f, 1, 1, 0,    // v1 (left)
     -0.5f, 0.5f, -0.5f, 0, 1, 0,   // v6
     -0.5f, -0.5f, -0.5f, 0, 0, 0,  // v7
     -0.5f, -0.5f, 0.5f, 1, 0, 0,   // v2

     -0.5f, -0.5f, -0.5f, 0, 0, 0,  // v7 (bottom)
     0.5f, -0.5f, -0.5f, 0, 0, 1,   // v4
     0.5f, -0.5f, 0.5f, 1, 0, 1,    // v3
     -0.5f, -0.5f, 0.5f, 1, 0, 0,   // v2

     0.5f, -0.5f, -0.5f, 0, 0, 1,   // v4 (back)
     -0.5f, -0.5f, -0.5f, 0, 0, 0,  // v7
     -0.5f, 0.5f, -0.5f, 0, 1, 0,   // v6
     0.5f, 0.5f, -0.5f, 0, 1, 1};   // v5

float _box_vertices_blender3[] =
    {1.0f, 1.0f, 1.0f, 1, 1, 1,    // v0 (front)
     -1.0f, 1.0f, 1.0f, 1, 1, 0,   // v1
     -1.0f, -1.0f, 1.0f, 1, 0, 0,  // v2
     1.0f, -1.0f, 1.0f, 1, 0, 1,   // v3

     1.0f, 1.0f, 1.0f, 1, 1, 1,    // v0 (right)
     1.0f, -1.0f, 1.0f, 1, 0, 1,   // v3
     1.0f, -1.0f, -1.0f, 0, 0, 1,  // v4
     1.0f, 1.0f, -1.0f, 0, 1, 1,   // v5

     1.0f, 1.0f, 1.0f, 1, 1, 1,    // v0 (top)
     1.0f, 1.0f, -1.0f, 0, 1, 1,   // v5
     -1.0f, 1.0f, -1.0f, 0, 1, 0,  // v6
     -1.0f, 1.0f, 1.0f, 1, 1, 0,   // v1

     -1.0f, 1.0f, 1.0f, 1, 1, 0,    // v1 (left)
     -1.0f, 1.0f, -1.0f, 0, 1, 0,   // v6
     -1.0f, -1.0f, -1.0f, 0, 0, 0,  // v7
     -1.0f, -1.0f, 1.0f, 1, 0, 0,   // v2

     -1.0f, -1.0f, -1.0f, 0, 0, 0,  // v7 (bottom)
     1.0f, -1.0f, -1.0f, 0, 0, 1,   // v4
     1.0f, -1.0f, 1.0f, 1, 0, 1,    // v3
     -1.0f, -1.0f, 1.0f, 1, 0, 0,   // v2

     1.0f, -1.0f, -1.0f, 0, 0, 1,   // v4 (back)
     -1.0f, -1.0f, -1.0f, 0, 0, 0,  // v7
     -1.0f, 1.0f, -1.0f, 0, 1, 0,   // v6
     1.0f, 1.0f, -1.0f, 0, 1, 1};   // v5

#endif /* CUBE_DATA_H */
