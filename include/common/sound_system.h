/*
 * File: sound_system.h
 * Project: common
 * File Created: Monday, 19th April 2021 5:30:54 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

/* Setup current targets that support sound with OpenAL */
#if defined(__linux__)
#define SOUND (1)
#endif
#if defined(WINDOWS)
#define SOUND (1)
#endif
#if defined(PSP)
/* Not yet */
#endif
#if defined(_arch_dreamcast)
/* Not yet */
#endif

#if SOUND
#include <AL/al.h>
#include <AL/alc.h>
#else
typedef unsigned int ALuint;
#endif

int SYS_SND_Setup(void);
int SYS_SND_Destroy(void);

void SND_Play(ALuint source);
int create_sound(ALuint *source, const char *filename);