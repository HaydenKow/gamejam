/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\file_access.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Saturday, July 6th 2019, 2:12:12 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include <stdlib.h>
#include <stdint.h>
#include <sys/stat.h>
#include <errno.h>
#include "file_access.h"

#if defined(_arch_dreamcast) || (defined(PSP) && defined(PSP_ISO))
char _path[256];

const char *transform_path(const char *path)
{
#ifdef _arch_dreamcast
    //Append /cd/ for dreamcast
    memset(_path, 0, sizeof(_path));
    snprintf(_path, 256, "/cd/%s", path); // /pc/ for dcload remote
#else
    //Append umd0:/ path for PSP
    memset(_path, 0, sizeof(_path));
    snprintf(_path, 256, "disc0:/PSP_GAME/USRDIR/%s", path);
#endif

    // Remove any duplicate '.' because iso9660 cant handle
    unsigned char i, count = 0, index = 0;
    for (i = 0, count = 0; _path[i]; i++)
    {
        if ((_path[i] == '.'))
        {
            count++;
            if (count == 1)
            {
                index = i;
            }
        }
    }
    if (count > 1)
    {
        _path[index] = '_';
    }
    return &_path[0];
}
#endif

int Sys_FileLength(FILE *f)
{
    int pos;
    int end;

    pos = ftell(f);
    fseek(f, 0, SEEK_END);
    end = ftell(f);
    fseek(f, pos, SEEK_SET);

    return end;
}

bool Sys_FileExists(const char *path)
{
    struct stat buffer;
    return (stat(path, &buffer) == 0);
}
