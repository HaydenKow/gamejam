#include <common/resource_manager.h>
#include <common/stack.h>
#include <scene/scene.h>

#include "common.h"
#include "image_loader.h"
#include "input.h"
#include "private.h"
#include "renderer.h"
#include "ui/ui_backend.h"

char error_str[64];
extern struct scene null_scene;

void Game_Main(__attribute__((unused)) int argc, __attribute__((unused)) char **argv)
{
  extern int main(int argc, char **argv);
  srand((unsigned)Sys_FloatTime());
  /* Start up Resource Manager */
  resource_test();

  RNDR_Init(SCR_WIDTH, SCR_HEIGHT);

  /* Can do setup here if you NEED/WANT it to run before anything else */
  UI_Init();

  //Setup our basic scene as bottom
  SCN_Push(null_scene);
}

void Game_Exit(void)
{
  SCN_Pop();

  resource_objects_empty();
}

void Host_Input(__attribute__((unused)) float time)
{
}

static inline void Host_Render3D(float time)
{
  RNDR_Reset();
  scene *scene_current = SCN_Current();

  if (scene_current->flags & SCENE_FALLTHROUGH_RENDER)
  {
    scene *next_scene = SCN_Peek();
    if (next_scene->render3D)
      (*next_scene->render3D)(time);
  }
  if (scene_current->render3D)
    (*scene_current->render3D)(time);
}

static inline void Host_Render2D(float time)
{
  UI_Set2D();
  UI_TextColorEx(1.0f, 1.0f, 1.0f, 1.0f);
  scene *scene_current = SCN_Current();

  if (scene_current->flags & SCENE_FALLTHROUGH_RENDER)
  {
    scene *next = SCN_Peek();
    if (next->render2D)
      (*next->render2D)(time);
  }
  if (scene_current->render2D)
    (*scene_current->render2D)(time);
}

void Host_Update(float time)
{
  scene *scene_current = SCN_Current();
  bool update_fallthrough = scene_current->flags & SCENE_FALLTHROUGH_UPDATE;

  if (scene_current->update)
    (*scene_current->update)(time);

  if (update_fallthrough)
  {
    scene *next = SCN_Peek();
    if (next->update)
      (*next->update)(time);
  }
}

void Host_Frame(float time)
{
  /* Render Both parts */
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // Clear The Screen And The Depth Buffer
  glLoadIdentity();                                   // Reset The View
  Host_Render3D(time);
  Host_Render2D(time);
}
