/*
 * Filename: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/jfm/JFM_loader.c
 * Path: /mnt/d/Dev/Dreamcast/UB_SHARE/gamejam/game/examples/placeholder/src/jfm
 * Created Date: Thursday, January 1st 1970, 12:00:00 am
 * Author: dev
 * 
 * Copyright (c) 2020 Hayden Kowalchuk
 * Copyright (c) 2006 Jake Stookey
 */

#include "jfm_loader.h"

#include <common/renderer.h>
#include <common/resource_manager.h>

extern char error_str[64];

static int LoadJFMAndConvert(const char *filename, model_jfm *jfm);

static model_jfm *JFM_load_internal(const char *path) {
  model_jfm *jfm = NULL;
  uint32_t crc = 0;
  int length = strlen(path);
  crc32(&crc, (uint8_t *)path, length);

  if (resource_object_find(crc) == -1) {
    jfm = (model_jfm *)malloc(sizeof(model_jfm));
    memset(jfm, 0, sizeof(model_jfm));

    /* Check if file even exists */
    if (!Sys_FileExists(path)) {
      return jfm;
    }

    jfm->crc = crc;
    LoadJFMAndConvert(path, jfm);
    if (jfm->vertices == NULL) {
#ifdef DEBUG
      printf("ERROR: for path(%s)\n", path);
#endif
      return jfm;
    }
#ifdef DEBUG
    printf("Adding new JFM! crc: %08X, %s\n", (unsigned int)crc, path);
#endif
    resource_object_add('j', crc, jfm);
    return jfm;
  } else {
#ifdef DEBUG
    printf("Found already loaded JFM! crc: %08X, %s\n", (unsigned int)crc, path);
#endif
    resource_object_add('j', crc, jfm);
    return (model_jfm *)(resource_object_pointer(crc));
  }
}

model_jfm *JFM_load(const char *path) {
  return JFM_load_internal(transform_path(path));
}

model_jfm *JFM_load_boolean(const char *path, bool transform) {
  return JFM_load_internal((transform) ? transform_path(path) : path);
}

/*@Todo: Rewrite this! */
void JFM_destroy(model_jfm *jfm) {
  if (jfm->vertices) {
    free(jfm->vertices);
    free(jfm->texturecoords);
    free(jfm->vertexindices);
    free(jfm->textureindices);
    jfm->vertices = NULL;
    jfm->texturecoords = NULL;
    jfm->vertexindices = NULL;
    jfm->textureindices = NULL;
  }
  memset(jfm, 0, sizeof(model_jfm));
  free(jfm);
}

static int LoadJFMAndConvert(const char *filename, model_jfm *jfm) {
  jfm->currentframe = 0;

  // Load the object file

  printf("Loading Object3d: %s\n", filename);

  FILE *fp = fopen(filename, "rb");
  if (fp == NULL) {
    printf("Error loading JFM!\n");
    return 0;
  }

  // Read 4 ints from data file
  // num vertices
  fread(&jfm->nv, sizeof(int), 1, fp);
  fread(&jfm->num_vertex_indices, sizeof(int), 1, fp);
  // Num texture coordinates
  fread(&jfm->nt, sizeof(int), 1, fp);
  fread(&jfm->num_texture_indices, sizeof(int), 1, fp);
  // Num frames in animation
  fread(&jfm->num_frames, sizeof(int), 1, fp);

  //printf("JFM Info:\n num vert: %d, vert ind: %d, num uv: %d, uv ind: %d. num frames: %d\n", jfm->nv, jfm->num_vertex_indices, jfm->nt, jfm->num_texture_indices, jfm->num_frames);

  // Allocate space for the arrays
  // allocate vertices for each animation frame
  jfm->vertices = (float *)malloc(sizeof(float) * jfm->nv * jfm->num_frames);
  jfm->vertexindices = (int32_t *)malloc(sizeof(int32_t) * jfm->num_vertex_indices);
  jfm->texturecoords = (float *)malloc(sizeof(float) * jfm->nt);
  jfm->textureindices = (int32_t *)malloc(sizeof(int32_t) * jfm->num_texture_indices);

  // Read arrays from data file
  fread(jfm->vertices, sizeof(float), jfm->nv * jfm->num_frames, fp);        /* 20 */
  fread(jfm->vertexindices, sizeof(int32_t), jfm->num_vertex_indices, fp);   /* 16652 */
  fread(jfm->texturecoords, sizeof(float), jfm->nt, fp);                     /* 18252 */
  fread(jfm->textureindices, sizeof(int32_t), jfm->num_texture_indices, fp); /* 20716 */

  fclose(fp);

  return 1;
}

void draw_jfm(model_jfm *jfm) {
  int i = 0, k;
  int num_verts = 0;

  int mode = GL_TRIANGLES;  // GL_QUADS or GL_TRIANGLES
  int prev_mode = GL_TRIANGLES;

  // Enable texture mapping if there's a tecture associated with the object
  if (jfm->texture) {
    glBindTexture(GL_TEXTURE_2D, jfm->texture);
    glEnable(GL_TEXTURE_2D);
  }

  glPushMatrix();
  glColor3f(1, 1, 1);
  glBegin(GL_TRIANGLES);  // Initially assume tri's and change it later if need be

  // Draw all the vertices
  while (i < jfm->num_vertex_indices) {
    // Determine mode  (quads or triangles)
    if (jfm->vertexindices[i + 3] == -1) {
      mode = GL_TRIANGLES;
      num_verts = 3;
    } else if (jfm->vertexindices[i + 4] == -1) {
      //printf("Error Quads!\n");
      mode = GL_QUADS;
      num_verts = 4;
    } else {
      printf("Unexpected error detecting mode - quads or tri's, i: %i, %i\n", i, jfm->vertexindices[i + 3]);
      break;
    }

    // If mode has changed, glEnd(); glBegin(mode)
    if (mode != prev_mode) {
      //Change mode!
      glEnd();
      glBegin(mode);
    }

    // Draw the vertex and texture
    for (k = 0; k < num_verts; k++) {
      if (jfm->num_texture_indices != 0) glTexCoord2f(jfm->texturecoords[(jfm->textureindices[i] * 2)], jfm->texturecoords[(jfm->textureindices[i] * 2 + 1)]);
      glVertex3f(jfm->vertices[(jfm->vertexindices[i] * 3) + (jfm->currentframe * jfm->nv)], jfm->vertices[(jfm->vertexindices[i] * 3 + 1) + (jfm->currentframe * jfm->nv)], jfm->vertices[(jfm->vertexindices[i] * 3 + 2) + (jfm->currentframe * jfm->nv)]);
      i++;
    }

    i++;  // One more increment for the "-1" value
    prev_mode = mode;
  }
  glEnd();

  glPopMatrix();
  if (jfm->texture) glDisable(GL_TEXTURE_2D);

  // Advance the animation frame for next time
  jfm->currentframe = (jfm->currentframe + 1) % jfm->num_frames;
}