/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2019 HaydenKow
 */

#include <common/common.h>
#include <scene/scene.h>
#include <ui/ui_backend.h>

static void Null_Update(float time);
static int is_loaded = 0;

struct scene null_scene = {
    .init = NULL,
    .render2D = NULL,
    .render3D = NULL,
    .update = &Null_Update,
    .flags = SCENE_BLOCK,
};

extern scene scene_start;

static void Null_Update(float time) {
  (void)time;
  if (!is_loaded) {
    is_loaded = 1;
    null_scene.update = NULL;
    SCN_ChangeTo(scene_start);
  }
}
