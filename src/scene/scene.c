/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\scene.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:34:47 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include <common/common.h>
#include <common/stack.h>
#include <common/resource_manager.h>

struct StackNode *scene_root;
scene scene_current;

scene *SCN_Push(scene next)
{
  push(&scene_root, next);
  return &scene_root->data;
}

scene *SCN_ChangeTo(scene next)
{
  pop(&scene_root);
  push(&scene_root, next);

  resource_objects_flush();
  return peek(&scene_root->next);
}

scene *SCN_Pop(void)
{
  pop(&scene_root);

  resource_objects_flush();
  return SCN_Current();
}

scene *SCN_Current(void)
{
  return peek(&scene_root);
}

scene *SCN_Peek(void)
{
  return peek(&scene_root->next);
}