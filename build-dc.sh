#!/bin/sh
docker run  --rm -v "$PWD:/src" -u `id -u`:`id -g`  haydenkow/nu-dcdev:release-4.7.3-rc3 make -j5 -f Makefile.dc "$@"