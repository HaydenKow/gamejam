/*
 * File: particle_system.h
 * Project: particles_basic
 * File Created: Sunday, 14th February 2021 9:14:38 pm
 * Author: Hayden Kowalchuk 
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */
#pragma once
#include <common/common.h>
#include <common/renderer.h>
#include <ui/gl_batcher.h>

typedef struct particle_s {
  struct {
    float x;
    float y;
  } pos;
  struct {
    float x;
    float y;
  } vel;
  float life;
  float original_life;
  float alpha;
  float size;
  float original_size;
  color_uc color;  //bgra
} particle_t;

static inline float coinFlip(void) {
  return (rand() > (RAND_MAX / 2) ? 1.0f : -1.0f);
}

static inline float Flip(float coin){
  return (coin > 0.5f ? 1.0f : -1.0f);
}

static inline float randf(void) {
  float x = ((float)rand() / (float)(RAND_MAX+1.0f));
  return x;
}

static inline float randf_range(float a) {
  return randf() * a;
}

void particle_init(unsigned int texture);
void particle_create(int index);
void particle_update(float dt);
void particle_draw(float dt);

int particle_get_alive(void);

void particle_create_single(void);
void particle_create_100_staggered(void);
void particle_create_100_simultaneous(void);