
#include "particle_basic_scene.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

#include "particle_system.h"

WINDOW_TITLE("particle_basic_scene", WINDOWED);

static void ParticleBasic_Init(void);
static void ParticleBasic_Exit(void);
static void ParticleBasic_Update(float time);
static void ParticleBasic_Render2D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&ParticleBasic_Init, &ParticleBasic_Exit, &ParticleBasic_Render2D, NULL, &ParticleBasic_Update, SCENE_BLOCK);
SCENE(scene_particle_basic, &ParticleBasic_Init, NULL, &ParticleBasic_Render2D, NULL, &ParticleBasic_Update, SCENE_BLOCK);

static tx_image *particle_texture;

static void ParticleBasic_Init(void) {
  particle_texture = IMG_load("particle.png");
  RNDR_CreateTextureFromImage(particle_texture);
  particle_init(particle_texture->id);
}

static void ParticleBasic_Exit(void) {
  resource_object_remove(particle_texture->crc);
}

static void ParticleBasic_Update(float time) {
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }
  if (INPT_ButtonEx(BTN_A, BTN_RELEASE)) {
    particle_create_single();
  }
  if (INPT_ButtonEx(BTN_B, BTN_RELEASE)) {
    particle_create_100_simultaneous();
  }
  particle_update(time);
}

static void ParticleBasic_Render2D(float time) {
  particle_draw(time);

  UI_TextColorEx(1, 1, 1, 1);
  UI_TextSize(14);
  {
    char buf[32];
    sprintf(buf, "# %d", particle_get_alive());
    UI_DrawString(14, 14, buf);
  }

  {
    char mspf[16];
    float current_mspf = ((1000.0f / time) / 1000.0f);
    snprintf(mspf, 16, "%2.02f fps", current_mspf > 60.0f ? 60.0f : current_mspf);
    UI_DrawStringCentered(640 / 2, 480 - 14, mspf);
  }
}
