/*
 * File: particle_basic_scene.h
 * Project: particles_basic
 * File Created: Sunday, 14th February 2021 8:53:49 pm
 * Author: Hayden Kowalchuk 
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once
#include <common.h>
#include <scene/scene.h>

extern scene scene_particle_basic;
