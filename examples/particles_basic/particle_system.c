/*
 * File: particle_system.c
 * Project: particles_basic
 * File Created: Sunday, 14th February 2021 9:14:32 pm
 * Author: Hayden Kowalchuk 
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */
#include "particle_system.h"

#include <ui/ui_backend.h>

#define MAX_PARTICLES (4096)

#ifdef _arch_dreamcast
glvert_fast_t _particle_verts[MAX_PARTICLES * 3];
#define _B (3)
#define _G (2)
#define _R (1)
#define _A (0)
#else
psp_fast_t _particle_verts[MAX_PARTICLES * 3];
#define _B (3)
#define _G (2)
#define _R (1)
#define _A (0)
#endif

static particle_t _particles[MAX_PARTICLES];
static unsigned int _texture;
static int particles_alive;

struct {
  float x;
  float y;
} starting_position;

void particle_init(unsigned int texture) {
  starting_position.x = (float)(640 / 2.0f);
  starting_position.y = (float)(480 / 2.0f);
  _texture = texture;
  memset(_particles, '\0', sizeof(_particles));
  for (int i = 0; i < MAX_PARTICLES; i++) {
    _particles[i].life = -1.0f;
  }
}

void particle_create(int index) {
  float angle = 90 + randf() * 180 * coinFlip();
  float speed = randf() * 60 + 20;
  float randomBlue = (randf() * 80) + 175;
  float size = randf() * 6 + 4;
  float life = randf() + 3;
#ifdef _arch_dreamcast
  unsigned int color_val = PACK_ARGB8888((uint8_t)(255 * 0.0f), (uint8_t)(255 * 0.0f),
                                         (uint8_t)(255 * 0.0f), (uint8_t)(randomBlue));
#else
  unsigned int color_val = PACK_BGRA8888((uint8_t)(255 * 0.0f), (uint8_t)(255 * 0.0f),
                                         (uint8_t)(255 * 0.0f), (uint8_t)(randomBlue));
#endif
  _particles[index] = (particle_t){.pos = {
                                       .x = starting_position.x,
                                       .y = starting_position.y,
                                   },
                                   .vel = {
                                       .x = speed * COS(DEG2RAD(angle)),
                                       .y = speed * SIN(DEG2RAD(angle)),
                                   },
                                   .color.packed = color_val,
                                   .life = life,
                                   .original_life = life,
                                   .size = size,
                                   .original_size = size};
}

void particle_update(float dt) {
  for (int index = 0; index < MAX_PARTICLES; index++) {
    if (_particles[index].life > 0.0f) {
      /* UPDATE */
      _particles[index].life -= dt;

      _particles[index].pos.x += _particles[index].vel.x * dt;
      _particles[index].pos.y += _particles[index].vel.y * dt;
      _particles[index].color.array[_B] = (uint8_t)(255 * (_particles[index].life / _particles[index].original_life));
#ifdef _arch_dreamcast
      //Vertex 1
      _particle_verts[(index * 3) + 0] = (glvert_fast_t){.flags = VERTEX, .vert = {_particles[index].pos.x, _particles[index].pos.y, 0}, .texture = {0, 0}, .color = {.packed = _particles[index].color.packed}, .pad0 = {0}};
      //Vertex 4
      _particle_verts[(index * 3) + 2] = (glvert_fast_t){.flags = VERTEX_EOL, .vert = {_particles[index].pos.x, _particles[index].pos.y + _particles[index].size, 0}, .texture = {0, 0 + 1}, .color = {.packed = _particles[index].color.packed}, .pad0 = {0}};
      //Vertex 2
      _particle_verts[(index * 3) + 1] = (glvert_fast_t){.flags = VERTEX, .vert = {_particles[index].pos.x + _particles[index].size, _particles[index].pos.y, 0}, .texture = {0 + 1, 0}, .color = {.packed = _particles[index].color.packed}, .pad0 = {0}};

#else
      //Vertex 1
      _particle_verts[(index * 3) + 0] = (psp_fast_t){.vert = {_particles[index].pos.x, _particles[index].pos.y, 0}, .texture = {0, 0}, .color = {.packed = _particles[index].color.packed}};
      //Vertex 4
      _particle_verts[(index * 3) + 2] = (psp_fast_t){.vert = {_particles[index].pos.x, _particles[index].pos.y + _particles[index].size, 0}, .texture = {0, 0 + 1}, .color = {.packed = _particles[index].color.packed}};
      //Vertex 2
      _particle_verts[(index * 3) + 1] = (psp_fast_t){.vert = {_particles[index].pos.x + _particles[index].size, _particles[index].pos.y, 0}, .texture = {0 + 1, 0}, .color = {.packed = _particles[index].color.packed}};

#endif
    }
  }
  for (int i = 0; i < MAX_PARTICLES && _particles[i].life > 0.0f; i++) {
    particles_alive = i + 1;
  }
}

void particle_draw(float dt) {
  (void)dt;

  glBindTexture(GL_TEXTURE_2D, _texture);
  glEnable(GL_BLEND);
  glEnable(GL_TEXTURE_2D);
  glDisable(GL_CULL_FACE);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
#ifdef _arch_dreamcast
  glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &_particle_verts[0].vert);
  glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &_particle_verts[0].texture);
  glColorPointer(GL_BGRA, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &_particle_verts[0].color);
#else
  //glInterleavedArrays(GL_T2F_C4UB_V3F, 0, &_particle_verts[0]);
  glVertexPointer(3, GL_FLOAT, sizeof(psp_fast_t), &_particle_verts[0].vert);
  glTexCoordPointer(2, GL_FLOAT, sizeof(psp_fast_t), &_particle_verts[0].texture);
  glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(psp_fast_t), &_particle_verts[0].color);
#endif

  glDrawArrays(GL_TRIANGLES, 0, (particles_alive)*3);

  glDisableClientState(GL_COLOR_ARRAY);
  glEnable(GL_CULL_FACE);
  glDisable(GL_BLEND);
}

void particle_create_single(void) {
  for (int i = 0; i < MAX_PARTICLES; i++) {
    _particles[i].life = -1.0f;
  }
  particle_create(0);
}

void particle_create_100_staggered(void) {
}

void particle_create_100_simultaneous(void) {
  for (int i = 0; i < MAX_PARTICLES; i++) {
    particle_create(i);
  }
}

int particle_get_alive(void) {
  return particles_alive;
}