/*
 * File: bar_scene.h
 * Project: sound_viz_basic
 * File Created: Monday, 29th March 2021 2:27:33 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

#include <common.h>

extern scene scene_bar;
