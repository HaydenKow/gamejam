/*
 * File: soundviz_bar.c
 * Project: sound_viz_basic
 * File Created: Monday, 29th March 2021 8:22:05 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/cube_data.h>
#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <common/sound_system.h>
#include <ui/ui_backend.h>

#include "bar_scene.h"

WINDOW_TITLE("BarViz | Integrity", WINDOWED);

static void SimpleViz_Init(void);
static void SimpleViz_Exit(void);
static void SimpleViz_Update(float time);
static void SimpleViz_Render2D(float time);
static void SimpleViz_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&SimpleViz_Init, &SimpleViz_Exit, &SimpleViz_Render2D, &SimpleViz_Render3D, &SimpleViz_Update, SCENE_BLOCK);
SCENE(scene_bar, &SimpleViz_Init, &SimpleViz_Exit, &SimpleViz_Render2D, &SimpleViz_Render3D, &SimpleViz_Update, SCENE_BLOCK);

extern int create_sound(ALuint *source, const char *filename);
ALuint s_song;
FILE *spectrum_file = NULL;
int spectrum_frames = 0;
int spectrum_bands = 0;
float *spectrum = NULL;
const int FRAME_SYNC_OFFSET = 0;  //30;

float *spectrum_buffer = NULL;

static float *get_spectrum_frame(int frame) {
  if (frame > spectrum_frames || frame < 0) {
    return spectrum_buffer;
  }
  /* OLD */
  //fseek(spectrum_file, (FRAME_SYNC_OFFSET * sizeof(float) * 10) + (frame * sizeof(float) * 10) + 8 /*header*/, SEEK_SET);
  //fread(dest, sizeof(float), 10, spectrum_file);
  /* NEW */
  return spectrum_buffer + (FRAME_SYNC_OFFSET + frame * spectrum_bands);
}

static void draw_cube(float x, float y, float z, float scale) {
  /* Setup VA params */
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glDisable(GL_TEXTURE_2D);
  glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
  glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

  /*Update Object Matrix */
  glPushMatrix();
  glTranslatef(x, y, z);
  //glRotatef(RAD2DEG(rot_x), 1, 0, 0);
  //glRotatef(RAD2DEG(rot_y), 0, 0, 1);
  //glRotatef(RAD2DEG(rot_z), 0, 1, 0);
  glScalef(1, scale, 1);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
  glPopMatrix();
}

static void SimpleViz_Init(void) {
  /* NEW */
  spectrum_file = fopen(transform_path("spectrum.bin"), "rb");
  fread(&spectrum_frames, sizeof(int), 1, spectrum_file);
  fread(&spectrum_bands, sizeof(int), 1, spectrum_file);

  printf("read %d frame with %d bands\n", spectrum_frames, spectrum_bands);

  /* Setup Buffers */
  spectrum_buffer = malloc(sizeof(float) * spectrum_frames * spectrum_bands);
  spectrum = malloc(sizeof(float) * spectrum_bands);

  fread(spectrum_buffer, sizeof(float), spectrum_bands * spectrum_frames, spectrum_file);
  fclose(spectrum_file);
  spectrum_file = NULL;

#ifndef _arch_dreamcast
  create_sound(&s_song, transform_path("track_01.wav"));
  alSourcePlay(s_song);
#else
  /* Use CDDA */

  cdrom_reinit();
  int status, disk_type;
  while (status == CD_STATUS_BUSY) {
    cdrom_get_status(&status, &disk_type);
  }
  cdrom_cdda_pause();
  cdrom_cdda_play(1, 1, 0xf, CDDA_TRACKS);
  cdrom_cdda_resume();
#endif
}

static void SimpleViz_Exit(void) {
}

static void SimpleViz_Update(float time) {
  (void)time;

  if (INPT_ButtonEx(BTN_A, BTN_RELEASE)) {
#if !defined(_arch_dreamcast)
    alSourceStop(s_song);
#else
    cdrom_cdda_pause();
#endif
  }

  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }

  spectrum = get_spectrum_frame(Sys_Frames());
}

static void SimpleViz_Render2D(float time) {
  (void)time;

  UI_TextSize(32);
  UI_DrawStringCentered(320, 440, "Spectrum Bars!");
}

/* 3D display tunables */
const float distance = 30.0f;
const float seperation = 3.0f;
const float offset = -14.0f;
const float height = -6.0f;
const int bands = 10;

/* Bam Bam */
//#define spectrum_scale_factor (1 / 16.0f)
/* Goymamba */
#define spectrum_scale_factor (1 / 16.0f)

static void SimpleViz_Render3D(float time) {
  (void)time;

  /* Simple visualizer for bands */
  float scale;  // *= (8.0f);
  for (int i = 0; i < bands; i++) {
    /* Read spectrum data and use */
    scale = spectrum[i] * spectrum_scale_factor;
    glPushMatrix();
    glTranslatef(offset + (seperation * i), height + scale, -distance);
    draw_cube(0, 0, 0, scale);
    glPopMatrix();
  }
}