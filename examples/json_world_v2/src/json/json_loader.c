#include "json_loader.h"

#include "jsmn.h"

static char asset_subdir[64];

void JSON_SetAssetDir(const char *dir) {
  strncpy(asset_subdir, dir, 63);
}

static int jsoneq(const char *json, jsmntok_t *tok, const char *s) {
  if (tok->type == JSMN_STRING && (int)strlen(s) == tok->end - tok->start &&
      strncmp(json + tok->start, s, tok->end - tok->start) == 0) {
    return 0;
  }
  return -1;
}

static __attribute__((unused)) const char *jsmn_type(int type) {
  switch (type) {
    case 1:
      return "JSMN_OBJECT";
      break;
    case 2:
      return "JSMN_ARRAY";
      break;
    case 3:
      return "JSMN_STRING";
      break;
    case 4:
      return "JSMN_PRIMITIVE";
      break;
  }
  return "";
}

int JSON_ParseDropper(const char *filename, json_object *objs, size_t max_objs, json_object *triggers, size_t max_triggers, json_object *camera) {
  unsigned int objects = 0;
  unsigned int cameras = 0;
  unsigned int current_trigger = 0;
  FILE *input = fopen(transform_path(filename), "rb");

  if (!input) {
    printf("Could not find file: %s\n", transform_path(filename));
    return 0;
  }

  fseek(input, 0L, SEEK_END);
  int flen = ftell(input);
  rewind(input);

  /* Create buffer */
  char *json_string = (char *)malloc(flen);
  memset(json_string, '\0', flen);

  /* Read our file */
  fread(json_string, flen, 1, input);
  fclose(input);

  int ret_tokens;
  jsmn_parser p;
  jsmntok_t tokens[2048]; /* We expect no more than 512 tokens */

  jsmn_init(&p);
  ret_tokens = jsmn_parse(&p, json_string, flen, tokens, sizeof(tokens) / sizeof(tokens[0]));
  if (ret_tokens < 0) {
    printf("Failed to parse JSON: %d\n", ret_tokens);
    return 0;
  }

  /* Assume the top-level element is an object */
  if (ret_tokens < 1 || tokens[0].type != JSMN_OBJECT) {
    printf("Object expected\n");
    return 0;
  }

  int root_index;
  /* Loop over all keys of the root object */
  for (root_index = 1; root_index < ret_tokens; root_index++) {
    if (jsoneq(json_string, &tokens[root_index], "entities") == 0) {
      /* entities is the main array, holding all entity objects */
      if (tokens[root_index + 1].type != JSMN_ARRAY) {
        continue; /* We expect entities to be an array of objects */
      }

      printf("Found %d entities\n", tokens[root_index + 1].size);

      /* Loop through all elements in the entity array */
      int entity_start = 0;

      for (int entity_num = 0; entity_num < tokens[root_index + 1].size; entity_num++) {
        int offset_to_next_entity = 0;
        jsmntok_t *entity = &tokens[root_index + entity_start + 2];
        //printf("at %d found type %s with size %d\n", root_index + entity_start + 2, jsmn_type(entity->type), entity->size);
        if (entity->type != JSMN_OBJECT) {
          printf("ERROR key: %.*s\n", entity->end - entity->start, json_string + entity->start);
          continue; /* We expect the entity to be an object */
        }

        bool isTrigger = false;

        for (int entity_elements = 0; entity_elements < entity->size; entity_elements++) {
          /* This is the whole object */
          //jsmntok_t *entity = &tokens[root_index + entity_start + 2 + offset_to_next_entity];
          //printf("key: %.*s\n", entity->end - entity->start, json_string + entity->start);

          /* This is the first token of the object */
          jsmntok_t *attrs = &tokens[root_index + entity_start + 3 + offset_to_next_entity];

          if (jsoneq(json_string, attrs, "name") == 0) {
            /* We may use strndup() to fetch string value */
            /*   %.*s = len, data */
            //printf("- Name: %.*s\n", (attrs + 1)->end - (attrs + 1)->start, json_string + (attrs + 1)->start);
            memcpy(objs[objects].name, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);

            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }
          if (jsoneq(json_string, attrs, "position") == 0) {
            jsmntok_t *position = attrs + 1;
            int position_size = position->size;
            position += 1;
            for (int pos_elem = 0; pos_elem < position_size; pos_elem++) {
              *(float *)(&objs[objects].pos.x + pos_elem) = dc_safe_atof(json_string + (position + pos_elem + 1)->start);
              position++;
            }
            float temp = objs[objects].pos.y;
            objs[objects].pos.y = objs[objects].pos.z;
            objs[objects].pos.z = -temp;
            offset_to_next_entity += position_size * 2 + 2;
            continue;
          }
          if (jsoneq(json_string, attrs, "rotation") == 0) {
            jsmntok_t *position = attrs + 1;
            int position_size = position->size;
            position += 1;
            for (int pos_elem = 0; pos_elem < position_size; pos_elem++) {
              *(float *)(&objs[objects].rot.x + pos_elem) = dc_safe_atof(json_string + (position + pos_elem + 1)->start);
              position++;
            }
            /* Translation */
            // x: pitch y: roll z: yaw
            //yaw   (rotation around)
            //pitch (looking up/down)
            //roll (rolling, not used for camera)

            /* We found a camera */
            if (strstr(objs[objects].name, "amera") != NULL) {
              objs[objects].rot.x *= -1;
              objs[objects].rot.x += M_PI / 2;
              objs[objects].rot.z *= -1;
              objs[objects].rot.z += M_PI / 2;
#ifdef DEBUG
              printf("Found Camera: %s\n", objs[objects].name);
#endif
            }
            if (objs[objects].rot.x < 0.0f) {
              objs[objects].rot.x += 2 * M_PI;
            }
            if (objs[objects].rot.y < 0.0f) {
              objs[objects].rot.y += 2 * M_PI;
            }
            if (objs[objects].rot.z < 0.0f) {
              objs[objects].rot.z += 2 * M_PI;
            }
            offset_to_next_entity += position_size * 2 + 2;
            continue;
          }

          if (jsoneq(json_string, attrs, "scale") == 0) {
            jsmntok_t *position = attrs + 1;
            int position_size = position->size;
            position += 1;
            for (int pos_elem = 0; pos_elem < position_size; pos_elem++) {
              *(float *)(&objs[objects].scale.x + pos_elem) = dc_safe_atof(json_string + (position + pos_elem + 1)->start);
              position++;
            }
            offset_to_next_entity += position_size * 2 + 2;
            continue;
          }

          if (jsoneq(json_string, attrs, "_mesh") == 0) {
            char temp[128] = {0};
            strcpy(temp, asset_subdir);
            strncat(temp, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            objs[objects].mesh = OBJ_load(temp);
            objs[objects].texture = NULL;
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          if (jsoneq(json_string, attrs, "_texture") == 0) {
            char temp[128] = {0};
            strcpy(temp, asset_subdir);
            strncat(temp, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            objs[objects].texture = IMG_load(temp);
            RNDR_CreateTextureFromImage(objs[objects].texture);
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          if (jsoneq(json_string, attrs, "_trigger") == 0) {
            memcpy(triggers[current_trigger].id, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            triggers[current_trigger].id[(attrs + 1)->end - (attrs + 1)->start] = '\0';
            printf("Found Trigger: %s\n", triggers[current_trigger].id);
            isTrigger = true;
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

          if (jsoneq(json_string, attrs, "_original_scale") == 0) {
            char temp[32] = {0};
            memcpy(temp, json_string + (attrs + 1)->start, (attrs + 1)->end - (attrs + 1)->start);
            objs[objects].orig_scale = dc_safe_atof(temp);
            offset_to_next_entity += 2; /* Key+Value */
            continue;
          }

#ifdef DEBUG
          printf("UNKNOWN key: %.*s\n", attrs->end - attrs->start, json_string + attrs->start);
#endif
          offset_to_next_entity += 2; /* Key+Value */
        }

        /* Copy to trigger and *erase* the object maybe */
        if (isTrigger) {
          memcpy(&triggers[current_trigger].name, &objs[objects].name, sizeof(objs[objects].name));
          /* copy pos, rot & scale */
          memcpy(&triggers[current_trigger].pos, &objs[objects].pos, 3 * sizeof(vec3f));
          triggers[current_trigger].type = objs[objects].type;
          triggers[current_trigger].mesh = NULL;
          triggers[current_trigger].texture = NULL;
          current_trigger++;
          /* if want to erase from objects -> objects--; */
        }
        isTrigger = false;

        objects++;
        entity_start += offset_to_next_entity + 1;
        if (objects >= max_objs) {
          break;
        }
      }
    } else {
#ifdef DEBUG
      printf("Unexpected key: %.*s\n", tokens[root_index].end - tokens[root_index].start, json_string + tokens[root_index].start);
#endif
      break;
    }
  }
  free(json_string);
#ifdef DEBUG
  printf("\nFinished parsing!\n\nObjects parsed:\n");
  for (unsigned int l = 0; l < objects; l++) {
    if (objs[l].texture != NULL && objs[l].mesh != NULL) {
      //objs[l].mesh->texture = objs[l].texture->id;
    }
    printf("name: %s id:%d\t", objs[l].name, l);
    printf("pos[%f, %f, %f]\t", objs[l].pos.x, objs[l].pos.y, objs[l].pos.z);
    printf("rot[%f, %f, %f]\t", RAD2DEG(objs[l].rot.x), RAD2DEG(objs[l].rot.y), RAD2DEG(objs[l].rot.z));
    printf("scale[%f, %f, %f]\n", objs[l].scale.x, objs[l].scale.y, objs[l].scale.z);
  }
#endif
  /*@Note: currently we dont force a camera */
  for (unsigned int l = 0; l < objects; l++) {
    if (strstr(objs[l].name, "amera") != NULL) {
      camera[cameras] = objs[l];
      printf("Camera: %s ID #%d\n", camera[cameras].name, cameras);
      cameras++;
    }
  }
  return objects;
}