/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "dialog_popup.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/renderer.h>
#include <ui/ui_backend.h>

#include "scenes.h"

static DialogCallback _callback;

static char dialog_title[32];
static char dialog_text[128];
static char dialog_yes[12];
static char dialog_no[12];
static int dialog_choice;
static float time_dialog;
static enum Dialog_Type dialog_type = Choice;
int dialog_answer = -1;

bool dialog_popup_active;

void DIALOG_Reset(void) {
  dialog_popup_active = false;
  dialog_type = Choice;
  dialog_answer = -1;
  dialog_choice = 0;
  time_dialog = 0;
}

void DIALOG_Draw(float time) {
  (void)time;
  if (dialog_popup_active) {
    /* Handle Input */
    {
      float now = Sys_FloatTime();

      /* Handle input every so often */
      if (now - time_dialog > 0.2f) {
        if (INPT_ButtonEx(BTN_A, BTN_RELEASE)) {
          dialog_answer = dialog_choice;
          dialog_popup_active = false;
          if (_callback != NULL)
            (*_callback)(dialog_answer);
        }

        /* position */
        if (dialog_type == Choice) {
          if (INPT_DPADDirection(DPAD_LEFT) || INPT_DPADDirection(DPAD_RIGHT)) {
            dialog_choice = !dialog_choice;
            time_dialog = now;
          }
        }
      }
    }

    UI_DrawFadeScreen();
    UI_TextColor(1, 1, 1);

    /* Draw Box */
    int height = 300;
    int width = (int)(height * 1.6f);
    int thickness = 4;

    dimen_RECT quad = {.x = 640 / 2 - width / 2, .y = 480 / 2 - height / 2, .w = width, .h = height};
    UI_DrawFill(&quad, 0.0f, 0.0f, 0.0f);

    quad.w = width;
    quad.h = thickness;
    UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Top
    quad.w = thickness;
    quad.h = height;
    UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Left
    quad.y = (480 / 2 + height / 2) - thickness;
    quad.w = width;
    quad.h = thickness;

    UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Bottom

    quad.x = 640 / 2 + width / 2;
    quad.y = 480 / 2 - height / 2;
    quad.w = thickness;
    quad.h = height;
    UI_DrawFill(&quad, 0, 255.0f, 255.0f);  //Right

    /* Now Text */
    UI_TextSize(28);
    UI_DrawStringCentered((640 / 2), (480 / 2) - (height / 2) + 20, dialog_title);
    UI_TextSize(24);
    //UI_DrawString_Linebreak((640 / 2) - (width / 2) + 8, (480 / 2) - (height / 2) + 64, dialog_text);
    UI_DrawString_Linebreak((640 / 2) - (width / 2) + 8, (480 / 2) - (height / 2) + 64, dialog_text, width);

    UI_TextSize(28);
    if (dialog_type == Choice) {
      /* Button background */
      int text_pixel_width = (strlen((dialog_choice ? dialog_yes : dialog_no)) * text_size) + 8;
      quad.x = (320 + (dialog_choice ? 64 : (-64))) - (text_pixel_width) / 2;
      quad.y = 320 - (text_size / 2) - 4;
      quad.w = text_pixel_width;
      quad.h = text_size + 8;
      UI_DrawFill(&quad, 0, 255.0f, 255.0f);
      /* Button Text */
      UI_DrawStringCentered(320 - 64, 320, dialog_no);
      UI_DrawStringCentered(320 + 64, 320, dialog_yes);
    } else {
      /* Button background */
      int text_pixel_width = (strlen(dialog_yes) * text_size) + 8;
      quad.x = 320 - (text_pixel_width) / 2;
      quad.y = 320 - (text_size / 2) - 4;
      quad.w = text_pixel_width;
      quad.h = text_size + 8;
      UI_DrawFill(&quad, 0, 255.0f, 255.0f);
      /* Button Text */
      UI_DrawStringCentered(320, 320, dialog_yes);
    }
  }
}

void DIALOG_SetTitle(const char *title) {
  strncpy(dialog_title, title, 32);
}

void DIALOG_SetDefaultTitle(void) {
  strcpy(dialog_title, "Are you sure?");
}

void DIALOG_SetText(const char *msg) {
  strncpy(dialog_text, msg, 128);
}

void DIALOG_SetOptions(const char *yes, const char *no) {
  strncpy(dialog_yes, yes, 12);
  strncpy(dialog_no, no, 12);
}
void DIALOG_SetDefaultOptions(void) {
  strcpy(dialog_yes, "Yes");
  strcpy(dialog_no, "No");
}

void DIALOG_SetCallback(DialogCallback cb) {
  _callback = cb;
}

int DIALOG_GetAnswer(void) {
  int temp = dialog_answer;
  if (dialog_answer != -1) {
    dialog_answer = -1;
  }
  return temp;
}

void DIALOG_SetType(enum Dialog_Type type) {
  dialog_type = type;
}