/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\dialog_popup.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Tuesday, November 12th 2019, 7:45:11 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#pragma once
#include <common.h>
#include <stdbool.h>

extern int dialog_answer;
extern bool dialog_popup_active;

typedef void (*DialogCallback)(int answer);
enum Dialog_Type {Choice = 1, Notice = 0};

static inline void DIALOG_SetVisible(bool visible) {
  dialog_popup_active = visible;
}

static inline bool DIALOG_IsVisible(void) {
  return dialog_popup_active;
}

void DIALOG_Reset(void);
void DIALOG_Draw(float time);
void DIALOG_SetVisible(bool visible);
void DIALOG_SetText(const char *msg);
void DIALOG_SetTitle(const char *title);
void DIALOG_SetDefaultTitle(void);
void DIALOG_SetOptions(const char *yes, const char *no);
void DIALOG_SetDefaultOptions(void);
void DIALOG_SetType(enum Dialog_Type type);
void DIALOG_SetCallback(DialogCallback cb);
int DIALOG_GetAnswer(void);