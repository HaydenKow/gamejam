/*
 * File: example_scene.c
 * Project: scenes
 * File Created: Tuesday, 2nd June 2020 5:16:20 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/image_loader.h>
#include <common/input.h>
#include <common/jfm_loader.h>
#include <common/math_headers.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <scene/scene.h>
#include <ui/ui_backend.h>

#include "../json/json_loader.h"
#include "cube_data.h"
#include "dialog_popup.h"

/* Dialog */
#include "../trigger/example.h"
#include "../trigger/response.h"

#define PLAYER_SCALE (0.064f)

extern GLfloat _box_vertices3[144];
extern GLfloat _box_vertices_blender3[144];
extern GLubyte _box_indices[36];

static void JsonExampleScene_Init(void);
static void JsonExampleScene_Exit(void);
static void JsonExampleScene_Update(float time);
static void JsonExampleScene_Render2D(float time);
static void JsonExampleScene_Render3D(float time);

WINDOW_TITLE("Json Example | Integrity", WINDOWED);
/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&JsonExampleScene_Init, &JsonExampleScene_Exit, &JsonExampleScene_Render2D, &JsonExampleScene_Render3D, &JsonExampleScene_Update, SCENE_BLOCK);
SCENE(game_example, &JsonExampleScene_Init, &JsonExampleScene_Exit, &JsonExampleScene_Render2D, &JsonExampleScene_Render3D, &JsonExampleScene_Update, SCENE_BLOCK);

static void Draw_Objects(void);
static void __attribute__((unused)) draw_cube(void);
static void draw_player(void);

static json_object objs[32];
static json_object triggers[32];
static json_object camera[3];
static json_object player;
static json_object cube;

static int last_hit_trigger;

enum Camera { Cam_Back = 0,
              Cam_Right = 1,
              Cam_Left = 2 };
static int current_camera = Cam_Back;

static int objects = 0;

static float level_adjust_angle;  // = (-M_PI / 2);  //-90.0f;

/* Callback */
void JsonExampleScene_ExampleCallback(int choice);

static void JsonExampleScene_Init(void) {
  player.mesh = OBJ_load("assets/basicCharacter.obj");
  player.texture = IMG_load("assets/skin_man.png");
  RNDR_CreateTextureFromImage(player.texture);
  player.mesh->texture = player.texture->id;
  strcpy(player.name, "player");
  player.pos = (vec3f){0, 0, 0};
  player.rot = (vec3f){0, 0, 0};
  player.scale = (vec3f){1, 1, 1};

  memset(objs, '\0', sizeof(objs));
  JSON_SetAssetDir("assets/example/");
  if (!(objects = JSON_ParseDropper("assets/levels/example.txt", objs, 32, triggers, 32, camera))) {
    printf("ERROR!\n");
  }

  for (int i = 0; i < 5; i++) {
    if (!stricmp(triggers[i].id, "load")) {
      player.pos = (vec3f){triggers[i].pos.x * (1 / PLAYER_SCALE), 0, triggers[i].pos.z * (1 / PLAYER_SCALE)};
      last_hit_trigger = i;
      break;
    }
  }

  level_adjust_angle = (M_PI / 2);  //90.0f;

  DIALOG_Reset();
}

static void JsonExampleScene_Exit(void) {
}

static void JsonExampleScene_Update(float time) {
  (void)time;
  if (DIALOG_IsVisible()) {
    return;
  }

#ifdef PSP
  static float speed = 1.0f;
#else
  static float speed = 0.5f;
#endif
  static float angle = 0.0f;

  {
    float rad = 0.0f;
    float _x = INPT_AnalogF(AXES_X);
    float _y = INPT_AnalogF(AXES_Y);

    float x = _x * SQRT(1 - _y * _y * 0.5f);
    float y = _y * SQRT(1 - _x * _x * 0.5f);

    rad = atan2f(y, x);
    angle = level_adjust_angle - rad;
    player.rot.z = angle;
#define DEADZONE_FORWARD (0.30f)
    if ((fabs(x) > DEADZONE_FORWARD) || (fabs(y) > DEADZONE_FORWARD)) {
      player.pos.x += (sinf(player.rot.z) * speed);
      player.pos.z += (cosf(player.rot.z) * speed);
    }

/* position */
#if defined(__MINGW32__) || defined(__linux__)
    if (INPT_DPADDirection(DPAD_LEFT)) {
      level_adjust_angle += 0.05f;
    }
    if (INPT_DPADDirection(DPAD_RIGHT)) {
      level_adjust_angle -= 0.05f;
    }
    if (INPT_DPADDirection(DPAD_UP)) {
      player.pos.x += (sinf(player.rot.z) * speed);
      player.pos.z += (cosf(player.rot.z) * speed);
    }
#endif
  }
}

static void JsonExampleScene_Render2D(float time) {
  (void)time;
  UI_TextSize(14);
  UI_TextColorEx(1, 1, 1, 1);
#ifdef DEBUG
  char msg[64];
#endif

  /* Player position */
  {
#if 0
    char msg[64];
    //snprintf(msg, 64, "player {%5.2f, %5.2f}", player.pos.x*0.065f, player.pos.z*0.065f);
    snprintf(msg, 64, "player {%5.2f, %5.2f}", player.pos.x, player.pos.z);
    UI_DrawStringCentered(640 / 2, 16, msg);
#endif
  }

  /* Trigger position */
  {
    int i = 0;
    bool hit_trigger = false;
    while (triggers[i].name[0] != '\0') {
      if ((player.pos.x > ((triggers[i].pos.x - triggers[i].scale.x) * (1 / PLAYER_SCALE)) && player.pos.x < ((triggers[i].pos.x + triggers[i].scale.x) * (1 / PLAYER_SCALE))) /* x*/
          && (player.pos.z > ((triggers[i].pos.z - triggers[i].scale.y) * (1 / PLAYER_SCALE)) && player.pos.z < ((triggers[i].pos.z + triggers[i].scale.y) * (1 / PLAYER_SCALE)))) /* y */ {
        /* Trigger Debugging */
        {
#ifdef DEBUG
          strcpy(msg, "Trigger: ");
          strcat(msg, triggers[i].id);
          UI_DrawStringCentered(640 / 2, 440, msg);

          snprintf(msg, 64, "trigger {%5.2f, %5.2f } {%5.2f, %5.2f}", triggers[i].pos.x * (1 / PLAYER_SCALE), triggers[i].pos.z * (1 / PLAYER_SCALE), triggers[i].scale.x * (1 / PLAYER_SCALE), triggers[i].scale.y * (1 / PLAYER_SCALE));
          UI_DrawStringCentered(640 / 2, 32, msg);
#endif
        }
        if (!stricmp(triggers[i].id, "load")) {
          i++;
          continue;
        }
        hit_trigger = true;
        if (i == last_hit_trigger) {
          break;
        }
        if (!stricmp(triggers[i].id, "choice")) {
          /* Dialog Popup */
          DIALOG_SetType(Choice);
          DIALOG_SetDefaultTitle();
          DIALOG_SetDefaultOptions();
          DIALOG_SetCallback(&JsonExampleScene_ExampleCallback);
          char temp[64];
          strncpy(temp, example_hit->responses[rand() % example_hit->num], 63);
          DIALOG_SetText(temp);
          DIALOG_SetVisible(true);
        }

        if (!stricmp(triggers[i].id, "example")) {
          /* Dialog Notice */
          DIALOG_SetType(Notice);
          DIALOG_SetTitle("Notice");
          DIALOG_SetOptions("Okay", "");
          DIALOG_SetCallback(NULL);
          char temp[64];
          strncpy(temp, example_hit->responses[rand() % example_hit->num], 63);
          DIALOG_SetText(temp);
          DIALOG_SetVisible(true);
        }

        if (!stricmp(triggers[i].id, "cam_left")) {
          current_camera = Cam_Left;
        }
        if (!stricmp(triggers[i].id, "cam_right")) {
          current_camera = Cam_Right;
        }
        if (!stricmp(triggers[i].id, "cam_back")) {
          current_camera = Cam_Back;
        }
        last_hit_trigger = i;
        break;
      }
      i++;
    }
    /* if not touching anything reset */
    if (!hit_trigger) {
      last_hit_trigger = -1;
    }
  }

  DIALOG_Draw(time);
}

void JsonExampleScene_ExampleCallback(int choice) {
  if (choice) {
    printf("Answered Positive!\n");
  } else {
    printf("Answered Negative!\n");
  }
}

static void JsonExampleScene_Render3D(float time) {
  static vec3f camera_front;
  static vec3f camera_look;
  glPushMatrix();
  glLoadIdentity();

  static float counter = 0.0f;
  counter += (time * 60);

  vec3f direction;
  direction.x = COS(camera[current_camera].rot.z) * COS(camera[current_camera].rot.x);
  direction.y = SIN(camera[current_camera].rot.x);
  direction.z = SIN(camera[current_camera].rot.z) * COS(camera[current_camera].rot.x);

  // x: pitch y: roll z: yaw
  glm_vec3_normalize((float *)&direction.x);
  camera_front = direction;

  glm_vec3_sub((float *)&camera[current_camera].pos.x, (float *)&camera_front.x, (float *)&camera_look.x);

  mat4 view;
  /* Old */
  //vec3f cameraUp = (vec3f){0.0f, 1.0f, 0.0f};
  //gluLookAt(camera.pos.x, camera.pos.y, camera.pos.z, player.pos.x * PLAYER_SCALE, 0, player.pos.z * PLAYER_SCALE, cameraUp.x, cameraUp.y, cameraUp.z);
  glm_lookat((float *)&camera[current_camera].pos, (vec3){player.pos.x * PLAYER_SCALE, 0, player.pos.z * PLAYER_SCALE}, GLM_YUP, view);
  glLoadMatrixf((float *)view);

  Draw_Objects();

  draw_player();

  glPopMatrix();
}

static void draw_cube(void) {
  /* Setup VA params */
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glDisable(GL_TEXTURE_2D);
  glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
  glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

  /*Update Object Matrix */
  glPushMatrix();
  glTranslatef(cube.pos.x, cube.pos.y, cube.pos.z);
  glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
  glPopMatrix();
}

static void draw_player(void) {
  glPushMatrix();
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
  if (player.texture != 0)
    glBindTexture(GL_TEXTURE_2D, player.texture->id);

  glEnable(GL_TEXTURE_2D);
  OBJ_bind(player.mesh);

  float normalizeAmount = PLAYER_SCALE;
  glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
  glTranslatef(player.pos.x, player.pos.y, player.pos.z);
  glRotatef(RAD2DEG(player.rot.z), 0, 1, 0);
  glDrawArrays(GL_TRIANGLES, 0, player.mesh->num_tris);
  glPopMatrix();
}

static void Draw_Objects(void) {
  if (objects <= 0) {
    return;
  }
  for (int i = 0; i < objects; i++) {
    if (objs[i].orig_scale == 0.0f) {
      objs[i].orig_scale = 1.0f;
    }
    /* Mesh */
    if (objs[i].mesh != NULL) {
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_COLOR_ARRAY);
      glEnableClientState(GL_TEXTURE_COORD_ARRAY);
      glDisable(GL_TEXTURE_2D);

      if (objs[i].texture != NULL) {
        GL_Bind(objs[i].texture);
        glEnable(GL_TEXTURE_2D);
      }
      OBJ_bind(objs[i].mesh);

      /*Update Object Matrix */
      glPushMatrix();
      glTranslatef(objs[i].pos.x, objs[i].pos.y, objs[i].pos.z);

      {
        glRotatef(RAD2DEG(objs[i].rot.x) - 90.0f, 1, 0, 0);
        //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
        glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
        glScalef(objs[i].scale.x / objs[i].orig_scale, objs[i].scale.y / objs[i].orig_scale, objs[i].scale.z / objs[i].orig_scale);
        glDrawArrays(GL_TRIANGLES, 0, objs[i].mesh->num_tris);
      }
      glPopMatrix();

    } else {
#if 1
      /* Cube */

      /* Setup VA params */
      glEnableClientState(GL_VERTEX_ARRAY);
      glEnableClientState(GL_COLOR_ARRAY);

      glDisableClientState(GL_TEXTURE_COORD_ARRAY);
      if (objs[i].texture != NULL) {
        glEnable(GL_TEXTURE_2D);
        GL_Bind(objs[i].texture);
      } else {
        glDisable(GL_TEXTURE_2D);
      }

      glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3 + 3);
      glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices_blender3);

      /*Update Object Matrix */
      glPushMatrix();
      glTranslatef(objs[i].pos.x, objs[i].pos.y, objs[i].pos.z);
      //glRotatef(RAD2DEG(objs[i].rot.x), 1, 0, 0);
      //glRotatef(RAD2DEG(objs[i].rot.y), 0, 0, 1);
      //glRotatef(RAD2DEG(objs[i].rot.z), 0, 1, 0);
      glScalef(objs[i].scale.x, objs[i].scale.z, objs[i].scale.y);
      glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
      glPopMatrix();
#endif
    }
  }
}
