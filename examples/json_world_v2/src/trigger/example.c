/*
 * File: example.c
 * Project: trigger
 * File Created: Tuesday, 2nd June 2020 5:16:20 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/common.h>

#include "response.h"

static const response_array _example_hit = {
    .num = 3,
    .responses = {
        "Example Message 1",
        "Perhaps String 2",
        "Even a third option!",
    },
};

const response_array *example_hit = &_example_hit;
