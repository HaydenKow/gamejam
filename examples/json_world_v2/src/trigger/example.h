/*
 * File: example.h
 * Project: trigger
 * File Created: Tuesday, 2nd June 2020 5:16:20 pm
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

struct response_array;
typedef struct response_array response_array;

extern const response_array *example_hit;
