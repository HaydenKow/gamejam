/*
 * File: response.h
 * Project: trigger
 * File Created: Thursday, 1st January 1970 12:00:00 am
 * Author: HaydenKow 
 * -----
 * Copyright (c) 2020 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

typedef struct response_array {
  const unsigned int num;
  const char* responses[];
} response_array;
