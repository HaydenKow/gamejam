/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\presenter\base.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\presenter
 * Created Date: Wednesday, January 22nd 2020, 6:18:04 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#ifndef BASE_H
#define BASE_H

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

#include "slide_message1.h"
#include "slide_message2.h"
#include "slide_title.h"
#include "slide_end.h"

#endif /* BASE_H */
