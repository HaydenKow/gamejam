/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world\chunk.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world
 * Created Date: Sunday, June 30th 2019, 7:58:31 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "chunk.h"

#include <assert.h>

extern chunk *WLRD_GetChunkAt(int x, int y, int z);

static const int transparent[16] = {2, 0, 0, 0, 1, 0, 0, 0, 3, 4, 0, 0, 0, 0, 0, 0};
/*static const char *blocknames[16] = {
	"air", "dirt", "topsoil", "grass", "leaves", "wood", "stone", "sand",
	"water", "glass", "brick", "ore", "woodrings", "white", "black", "x-y"};*/

static const voxel voxel_types[6] = {
    /* air */ {0, 0},
    /* dirt */ {.top_index = 2, .side_index = 2},
    /* grass */ {.top_index = 15, .side_index = 3},
    /* diamond */ {.top_index = 39, .side_index = 42},
};

chunk CHNK_create(int16_t x, int16_t z) {
  chunk _chunk;
  memset(&_chunk, 0, sizeof(chunk));
  _chunk.changed = true;
  _chunk.worldX = x;
  _chunk.worldZ = z;
  return _chunk;
}

void CHNK_delete(chunk *chunk_temp) {
  if (chunk_temp->_buffer)
    free(chunk_temp->_buffer);

  memset(chunk_temp, 0, sizeof(chunk));

  chunk_temp->changed = true;
}

#if 0
void CHNK_setup(chunk *_chunk)
{
	*_chunk = CHNK_create();

	for (int x = 0; x < CX; x++)
	{
		for (int y = 0; y < CY; y++)
		{
			for (int z = 0; z < CZ; z++)
			{
#if 0
                char type = 0;
                if ((x <= 0 || z <= 0 || x >= 15) && y < 15)
                {
                    type = 1;
                }
                if (y <= 1)
                {
                    type = 1;
                }
#else
				char type = 1;
				if (y > ((2 * SIN(x)) + (2 * SIN(z))))
				{
					type = 0;
				}
				/*char type = 0;
                if ((x == 8) && y < 8)
                {
                    type = 1;
                }*/
#endif
				if (y == 0)
				{
					type = 1;
				}
				CHNK_set(_chunk, x, y, z, type);
			}
		}
	}

	CHNK_update(_chunk);
}
#endif

uint8_t CHNK_get(chunk *_chunk, int x, int y, int z) {
  /* @Todo: Error Checking */
  /*assert(x < CX);
	assert(y < CY);
	assert(z < CZ);
	assert(x >= 0);
	assert(y >= 0);
	assert(z >= 0);*/
  return _chunk->blk[x % CX][y % CY][z % CZ];
}

void CHNK_set(chunk *_chunk, int x, int y, int z, uint8_t type) {
  _chunk->blk[x][y][z] = type;
  _chunk->changed = true;
}

inline bool CHNK_isblocked(chunk *_chunk, int x1, int y1, int z1, int x2, int y2, int z2) {
  // Invisible blocks are always "blocked"
  if (!_chunk->blk[x1][y1][z1])
    return true;

  // Leaves do not block any other block, including themselves
  //if (transparent[CHNK_get(_chunk, x2, y2, z2)] == 1)
  //	return false;

  // Non-transparent blocks always block line of sight
  if (!transparent[CHNK_get(_chunk, x2 & (CX - 1), y2 & (CY - 1), z2 & (CZ - 1))])
    return true;

  // Otherwise, LOS is only blocked by blocks if the same transparency type
  return transparent[CHNK_get(_chunk, x2 & (CX - 1), y2 & (CY - 1), z2 & (CZ - 1))] == transparent[_chunk->blk[x1][y1][z1]];
}

inline bool CHNK_isSolid(chunk *_chunk, int x, int y, int z) {
  if (!_chunk || !_chunk->blocks) {
    return true;
  }

  if (y > CY - 1) {
    return true;  //return CHNK_isSolid(WLRD_GetChunkAt(x, y + 1, z), x, y - CY, z);
  }

  if (x > CX - 1) {
    return CHNK_isSolid(WLRD_GetChunkAt(x + 1, y, z), x - CX, y, z);
  }

  if (z > CZ - 1) {
    return CHNK_isSolid(WLRD_GetChunkAt(x, y, z + 1), x, y, z - CZ);
  }

  if (y < 0) {
    return true;  //return CHNK_isSolid(WLRD_GetChunkAt(x, y - 1, z), x, y + CY, z);
  }

  if (x < 0) {
    return CHNK_isSolid(WLRD_GetChunkAt(x - 1, y, z), x + CX, y, z);
  }

  if (z < 0) {
    return CHNK_isSolid(WLRD_GetChunkAt(x, y, z - 1), x, y, z + CZ);
  }

  return (CHNK_get(_chunk, x & (CX - 1), y & (CY - 1), z & (CZ - 1)) != 0);
}

inline bool CHNK_isLiquid(__attribute__((unused)) chunk *_chunk, __attribute__((unused)) int x1, __attribute__((unused)) int y1, __attribute__((unused)) int z1) {
  return false;
}

void CHNK_update(chunk *_chunk) {
  /* Pass 1 */
  /* We loop through one time to figure out exactly where our boundaries are and how big to make our buffer */
  _chunk->blocks = 0;
  _chunk->left_index = 0;
  _chunk->right_index = 0;
  _chunk->front_index = 0;
  _chunk->back_index = 0;
  _chunk->top_index = 0;
  _chunk->bottom_index = 0;

  for (int x = 0; x < CX; x++) {
    for (int y = 0; y < CY; y++) {
      for (int z = 0; z < CZ; z++) {
        if (!_chunk->blk[x][y][z])
          continue;
        _chunk->blocks++;

        // Check to see if the neighbours are empty and draw faces.
        if (x > 0) {
          if (!CHNK_isblocked(_chunk, x, y, z, x - 1, y, z)) {
            _chunk->left_index += 6;
          }
        } else
        //else if (not leftNeighbourIsSolid(chunk_size - 1, y, z))
        {
          _chunk->left_index += 6;
        }

        if (x < CHNK_SIZE - 1) {
          if (!CHNK_isblocked(_chunk, x, y, z, x + 1, y, z)) {
            _chunk->right_index += 6;
          }
        } else
        //else if (not rightNeighbourIsSolid(0, y, z))
        {
          _chunk->right_index += 6;
        }

        if (y > 0) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y - 1, z)) {
            _chunk->bottom_index += 6;
          }
        }
        /*else if (not bottomNeighbourIsSolid(x, chunk_size - 1, z))
				{
					addFace(_chunk, x, y, z, BELOW, 1);
				}*/

        if (y < CHNK_SIZE - 1) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y + 1, z)) {
            _chunk->top_index += 6;
          }
        } else
        //else if (not topNeighbourIsSolid(x, 0, z))
        {
          _chunk->top_index += 6;
        }

        if (z > 0) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y, z - 1)) {
            _chunk->back_index += 6;
          }
        } else
        //else if (not backNeighbourIsSolid(x, y, chunk_size - 1))
        {
          _chunk->back_index += 6;
        }

        if (z < CHNK_SIZE - 1) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y, z + 1)) {
            _chunk->front_index += 6;
          }
        }
        //else if (not frontNeighbourIsSolid(x, y, 0))
        else {
          _chunk->front_index += 6;
        }
      }
    }
  }
  _chunk->_total_elements = (_chunk->left_index + _chunk->right_index + _chunk->top_index + _chunk->bottom_index + _chunk->front_index + _chunk->back_index);

  _chunk->_buffer = malloc(_chunk->_total_elements * sizeof(glvert_fast_t));

  _chunk->changed = false;

  _chunk->top = _chunk->_buffer;
  _chunk->bottom = _chunk->top + _chunk->top_index;
  _chunk->left = _chunk->bottom + _chunk->bottom_index;
  _chunk->right = _chunk->left + _chunk->left_index;
  _chunk->front = _chunk->right + _chunk->right_index;
  _chunk->back = _chunk->front + _chunk->front_index;
  _chunk->left_index = 0;
  _chunk->right_index = 0;
  _chunk->front_index = 0;
  _chunk->back_index = 0;
  _chunk->top_index = 0;
  _chunk->bottom_index = 0;

  for (int x = 0; x < CX; x++) {
    for (int y = 0; y < CY; y++) {
      for (int z = 0; z < CZ; z++) {
        if (!_chunk->blk[x][y][z])
          continue;

        if (_chunk->blk[x][y + 1][z] == 0 && _chunk->blk[x][y][z] == 1) {
          _chunk->blk[x][y][z] = 2;
        }

        // Check to see if the neighbours are empty and draw faces.
        if (x > 0) {
          if (!CHNK_isblocked(_chunk, x, y, z, x - 1, y, z)) {
            CHNK_addFace(_chunk, x, y, z, LEFT, 1);
          }
        } else
        //else if (not leftNeighbourIsSolid(chunk_size - 1, y, z))
        {
          CHNK_addFace(_chunk, x, y, z, LEFT, 1);
        }

        if (x < CHNK_SIZE - 1) {
          if (!CHNK_isblocked(_chunk, x, y, z, x + 1, y, z)) {
            CHNK_addFace(_chunk, x, y, z, RIGHT, 1);
          }
        } else
        //else if (not rightNeighbourIsSolid(0, y, z))
        {
          CHNK_addFace(_chunk, x, y, z, RIGHT, 1);
        }

        if (y > 0) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y - 1, z)) {
            CHNK_addFace(_chunk, x, y, z, BELOW, 1);
          }
        }
        /*else if (not bottomNeighbourIsSolid(x, chunk_size - 1, z))
				{
					addFace(_chunk, x, y, z, BELOW, 1);
				}*/

        if (y < CHNK_SIZE - 1) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y + 1, z)) {
            CHNK_addFace(_chunk, x, y, z, ABOVE, 1);
          }
        } else
        //else if (not topNeighbourIsSolid(x, 0, z))
        {
          CHNK_addFace(_chunk, x, y, z, ABOVE, 1);
        }

        if (z > 0) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y, z - 1)) {
            CHNK_addFace(_chunk, x, y, z, BACK, 1);
          }
        } else
        //else if (not backNeighbourIsSolid(x, y, chunk_size - 1))
        {
          CHNK_addFace(_chunk, x, y, z, BACK, 1);
        }

        if (z < CHNK_SIZE - 1) {
          if (!CHNK_isblocked(_chunk, x, y, z, x, y, z + 1)) {
            CHNK_addFace(_chunk, x, y, z, FRONT, 1);
          }
        }
        //else if (not frontNeighbourIsSolid(x, y, 0))
        else {
          CHNK_addFace(_chunk, x, y, z, FRONT, 1);
        }
      }
    }
  }

#if 0
	//printf("Final count elements:%d blocks:%d\n", _chunk->elements, _chunk->blocks);
	printf("Final count elements: %d and blocks: %d\n", _chunk->_total_elements, _chunk->blocks);
	printf("\tleft: %d\n", _chunk->left_index);
	printf("\tright: %d\n", _chunk->right_index);
	printf("\ttop: %d\n", _chunk->top_index);
	printf("\tbottom: %d\n", _chunk->bottom_index);
	printf("\tfront: %d\n", _chunk->front_index);
	printf("\tback: %d\n", _chunk->back_index);
	printf("memory used: %d bytes || %d kb\n", _chunk->_total_elements * (int)sizeof(glvert_fast_t), (int)(_chunk->_total_elements * sizeof(glvert_fast_t)) / 1024);
#endif
}

static inline void _addVert(glvert_fast_t *vert, glvert_fast_t *temp, float x, float y, float z, float texw, float texh) {
  vert->vert.x = temp->vert.x + x;
  vert->vert.y = temp->vert.y + y;
  vert->vert.z = temp->vert.z + z;
  vert->texture.u = temp->texture.u + texw;
  vert->texture.v = temp->texture.v + texh;
}

// Create a cube face for the given voxel.
void CHNK_addFace(chunk *_chunk, float xf, float yf, float zf, facePos facing, short size) {
  glvert_fast_t v = {.flags = 0, .vert = {xf, yf, zf}, .texture = {0, 0}, .color = {{255, 255, 255, 255}}, .pad0 = {0}};
  short x = (short)xf;
  short y = (short)yf;
  short z = (short)zf;

  // Add a new face.
  switch (facing) {
    case ABOVE:
      v.texture.u = ATLAS_X(voxel_types[_chunk->blk[x][y][z]].top_index);
      v.texture.v = ATLAS_Y(voxel_types[_chunk->blk[x][y][z]].top_index);
      _addVert(&_chunk->top[_chunk->top_index++], &v, size, size, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //C
      _addVert(&_chunk->top[_chunk->top_index++], &v, size, size, 0, ATLAS_TEX_WIDTH, 0);                    //D
      _addVert(&_chunk->top[_chunk->top_index++], &v, 0, size, size, 0, ATLAS_TEX_HEIGHT);                   //B
      _addVert(&_chunk->top[_chunk->top_index++], &v, 0, size, size, 0, ATLAS_TEX_HEIGHT);                   //B
      _addVert(&_chunk->top[_chunk->top_index++], &v, size, size, 0, ATLAS_TEX_WIDTH, 0);                    //D
      _addVert(&_chunk->top[_chunk->top_index++], &v, 0, size, 0, 0, 0);                                     //A
      break;

    case BELOW:
      v.texture.u = ATLAS_X(voxel_types[_chunk->blk[x][y][z]].top_index);
      v.texture.v = ATLAS_Y(voxel_types[_chunk->blk[x][y][z]].top_index);
      _addVert(&_chunk->bottom[_chunk->bottom_index++], &v, size, 0, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //C
      _addVert(&_chunk->bottom[_chunk->bottom_index++], &v, 0, 0, size, ATLAS_TEX_WIDTH, 0);                    //B
      _addVert(&_chunk->bottom[_chunk->bottom_index++], &v, size, 0, 0, 0, ATLAS_TEX_HEIGHT);                   //D
      _addVert(&_chunk->bottom[_chunk->bottom_index++], &v, size, 0, 0, 0, ATLAS_TEX_HEIGHT);                   //D
      _addVert(&_chunk->bottom[_chunk->bottom_index++], &v, 0, 0, size, ATLAS_TEX_WIDTH, 0);                    //B
      _addVert(&_chunk->bottom[_chunk->bottom_index++], &v, 0, 0, 0, 0, 0);                                     //A
      break;
    case LEFT:
      v.texture.u = ATLAS_X(voxel_types[_chunk->blk[x][y][z]].side_index);
      v.texture.v = ATLAS_Y(voxel_types[_chunk->blk[x][y][z]].side_index);
      _addVert(&_chunk->left[_chunk->left_index++], &v, 0, size, size, ATLAS_TEX_WIDTH, 0);              //A
      _addVert(&_chunk->left[_chunk->left_index++], &v, 0, size, 0, 0, 0);                               //B
      _addVert(&_chunk->left[_chunk->left_index++], &v, 0, 0, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //D
      _addVert(&_chunk->left[_chunk->left_index++], &v, 0, 0, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //D
      _addVert(&_chunk->left[_chunk->left_index++], &v, 0, size, 0, 0, 0);                               //B
      _addVert(&_chunk->left[_chunk->left_index++], &v, 0, 0, 0, 0, ATLAS_TEX_HEIGHT);                   //A
      break;
    case RIGHT:
      v.texture.u = ATLAS_X(voxel_types[_chunk->blk[x][y][z]].side_index);
      v.texture.v = ATLAS_Y(voxel_types[_chunk->blk[x][y][z]].side_index);
      _addVert(&_chunk->right[_chunk->right_index++], &v, size, size, size, ATLAS_TEX_WIDTH, 0);              //B
      _addVert(&_chunk->right[_chunk->right_index++], &v, size, 0, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //C
      _addVert(&_chunk->right[_chunk->right_index++], &v, size, size, 0, 0, 0);                               //A
      _addVert(&_chunk->right[_chunk->right_index++], &v, size, size, 0, 0, 0);                               //A
      _addVert(&_chunk->right[_chunk->right_index++], &v, size, 0, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //C
      _addVert(&_chunk->right[_chunk->right_index++], &v, size, 0, 0, 0, ATLAS_TEX_HEIGHT);                   //D
      break;
    case FRONT:
      v.texture.u = ATLAS_X(voxel_types[_chunk->blk[x][y][z]].side_index);
      v.texture.v = ATLAS_Y(voxel_types[_chunk->blk[x][y][z]].side_index);
      _addVert(&_chunk->front[_chunk->front_index++], &v, size, size, size, ATLAS_TEX_WIDTH, 0);              //A
      _addVert(&_chunk->front[_chunk->front_index++], &v, 0, size, size, 0, 0);                               //B
      _addVert(&_chunk->front[_chunk->front_index++], &v, 0, 0, size, 0, ATLAS_TEX_HEIGHT);                   //C
      _addVert(&_chunk->front[_chunk->front_index++], &v, 0, 0, size, 0, ATLAS_TEX_HEIGHT);                   //C
      _addVert(&_chunk->front[_chunk->front_index++], &v, size, 0, size, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //D
      _addVert(&_chunk->front[_chunk->front_index++], &v, size, size, size, ATLAS_TEX_WIDTH, 0);              //A
      break;
    case BACK:
      v.texture.u = ATLAS_X(voxel_types[_chunk->blk[x][y][z]].side_index);
      v.texture.v = ATLAS_Y(voxel_types[_chunk->blk[x][y][z]].side_index);
      _addVert(&_chunk->back[_chunk->back_index++], &v, 0, 0, 0, 0, ATLAS_TEX_HEIGHT);                   //D
      _addVert(&_chunk->back[_chunk->back_index++], &v, 0, size, 0, 0, 0);                               //A
      _addVert(&_chunk->back[_chunk->back_index++], &v, size, size, 0, ATLAS_TEX_WIDTH, 0);              //B
      _addVert(&_chunk->back[_chunk->back_index++], &v, size, size, 0, ATLAS_TEX_WIDTH, 0);              //B
      _addVert(&_chunk->back[_chunk->back_index++], &v, size, 0, 0, ATLAS_TEX_WIDTH, ATLAS_TEX_HEIGHT);  //C
      _addVert(&_chunk->back[_chunk->back_index++], &v, 0, 0, 0, 0, ATLAS_TEX_HEIGHT);                   //D
      break;
    default:
      break;
  }
}

void CHNK_render(chunk *_chunk) {
  if (_chunk->changed)
    CHNK_update(_chunk);
}