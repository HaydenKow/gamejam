/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\player.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Monday, July 8th 2019, 11:00:54 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "player.h"

#include <common/resource_manager.h>

#include "input.h"
#include "obj_loader.h"
#include "renderer.h"

extern GLfloat _box_vertices3[144];
extern GLubyte _box_indices[36];

static AABB aabb;
static bool canJump = true;

static model_obj *player_obj;
static tx_image *player_tx;
rigidbody *player;

void PLYR_Draw() {
  if (!player) {
    return;
  }

  vec3 position;
  RBDY_GetPosition(player, position);

  if (player_obj->num_faces > 0) {
    glEnable(GL_TEXTURE_2D);
    glDisable(GL_BLEND);
    glDisableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glBindTexture(GL_TEXTURE_2D, player_obj->texture);

    glPushMatrix();
    OBJ_bind(player_obj);

    glTranslatef(position[0], position[1], position[2]);

    float normalizeAmount = MIN(1 / (player_obj->max[0] - player_obj->min[0]), 1 / (player_obj->max[2] - player_obj->min[2]));
    glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
    glTranslatef(2.0f, 0, 2.0f);
    glDrawArrays(GL_TRIANGLES, 0, player_obj->num_tris);
    glPopMatrix();
  } else {
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);
    glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3 + 3);
    glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3);
    glDisable(GL_TEXTURE_2D);

    glPushMatrix();
    glTranslatef(position[0], position[1], position[2]);
    glScalef(aabb.vec[0], aabb.vec[1], aabb.vec[2]);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
    glPopMatrix();
  }
}

static void PLYR_ResetJump(__attribute__((unused)) rigidbody *body, vec3 impacts) {
  canJump = impacts[1] > 0.0f;
}

static int assets_loaded = 0;

void PLYR_Create() {
  //aabb = AABB_Create((vec3){0.0f, 8.0f, 8.0f}, (vec3){0.8f, 2.0f, 0.8f});
  aabb = AABB_Create((vec3){0.0f, 8.0f, 8.0f}, (vec3){1.0f, 2.0f, 1.0f});
  player = PHYS_AddRigidbodyEx(&aabb, 5.0f, 0.6f, 0.91f, 1.0f, NULL);
  player->onCollide = &PLYR_ResetJump;

  if (!assets_loaded) {
    /* Attempt to load a model */
    player_obj = OBJ_load("assets/basicCharacter.obj");
    assets_loaded = 1;
  }

  player_tx = IMG_load("assets/skin_man.png");
  if (resource_object_count(player_tx->crc) == 1) {
    /* Ensure we never Free */
    IMG_load("assets/skin_man.png");
  }
  RNDR_CreateTextureFromImage(player_tx);
  player_obj->texture = player_tx->id;
}

void PLYR_Destroy() {
  resource_object_remove(player_tx->crc);
  /* We dont actually destroy anything now, we keep after loaded */
  /*
    OBJ_destroy(&player_obj);
    IMG_destroy(&player_tx);
    */
}

static float lastUpdate = 0.0f;
void PLYR_Update(void) {
  vec3 zdirection;
  vec3 speed;
  vec3 position;
  glm_vec3_copy(GLM_VEC3_ZERO, position);
  glm_vec3_scale(GLM_VEC3_ONE, 1.0f, speed);
  if (INPT_DPADDirection(DPAD_UP_HELD)) {
    glm_vec3_scale(GLM_ZUP, -1, zdirection);
    glm_vec3_muladd(zdirection, GLM_VEC3_ONE, position);
  }

  if (INPT_DPADDirection(DPAD_DOWN_HELD)) {
    glm_vec3_muladd(GLM_ZUP, GLM_VEC3_ONE, position);
  }

  float now = Sys_FloatTime();
  if (now - lastUpdate > 0.25f) {
    if (INPT_Button(BTN_A)) {
      RBDY_SetPosition(player, (vec3){7, 8, 8});
      lastUpdate = now;
    }

    if (INPT_Button(BTN_B)) {
      if (canJump)
        RBDY_ApplyImpulse(player, (vec3){0, 35, 0});
      canJump = false;
      lastUpdate = now;
    }
  }

  if (INPT_DPADDirection(DPAD_LEFT_HELD)) {
    glm_vec3_scale(GLM_XUP, -1, zdirection);
    glm_vec3_muladd(zdirection, GLM_VEC3_ONE, position);
  }

  if (INPT_DPADDirection(DPAD_RIGHT_HELD)) {
    glm_vec3_muladd(GLM_XUP, GLM_VEC3_ONE, position);
  }
  glm_normalize(position);
  glm_vec3_mul(position, speed, position);
  RBDY_ApplyImpulse(player, position);
}
