/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\boxes[i].c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common
 * Created Date: Monday, July 8th 2019, 11:00:54 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "box.h"

#include <common/cube_data.h>

#include "2d_physics/2d_physics.h"
#include "input.h"
#include "renderer.h"

extern int boxes_2d;
extern rigidbody_2d **bodies_2d;

#if 0
#define BOXES 144
static int _boxes = 0;
rigidbody *boxes[BOXES];
#endif

void BOX_Draw() {
#if 0
    if (boxes_2d <= 0)
    {
        return;
    }

    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisable(GL_TEXTURE_2D);
    glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3 + 3);
    glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), _box_vertices3);

    for (int i = 0; i < boxes_2d - 2; i++)
    {
        glPushMatrix();
        //float x = 2*(bodies_2d[i]->shape.max.x - bodies_2d[i]->shape.min.x) / 2 + bodies_2d[i]->shape.min.x;
        //float y = 480 - ((bodies_2d[i]->shape.max.y - bodies_2d[i]->shape.min.y) / 2 + bodies_2d[i]->shape.min.y);

        float width = RBDY_2D_GetWidth(bodies_2d[i]);
        float height = RBDY_2D_GetHeight(bodies_2d[i]);

        vec3 position;
        RBDY_2D_GetPosition_OGL(bodies_2d[i], position);
        if (width > 25)
        {
            //x=0;
            //printf("pos: {%f, %f, %f}\n", position[0], position[1], position[2]);
        }
        //glTranslatef(x / 32, y / 24, 0);
        //glScalef(width / 25, height / 25, 1);
        glTranslatef(position[0], position[1], position[2]);

        //
        //glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);
        //glTranslatef(-width * (32.0f/25.0f) / 2.0f, 0, 0);
        glScalef(width / 25.0f, height / 25.0f, 1);
        //printf("position: %f, adjustment: %f\n", position[0], position[0] - (width * (32.0f/25.0f) / 2.0f));
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);

        glPopMatrix();
    }

    glPushMatrix();
    float width = RBDY_2D_GetWidth(bodies_2d[boxes_2d - 2]);
    float height = RBDY_2D_GetHeight(bodies_2d[boxes_2d - 2]);
    vec3 position;
    RBDY_2D_GetPosition_OGL(bodies_2d[boxes_2d - 2], position);

    glTranslatef(position[0], position[1] - 3, position[2] + 0.1f);
    glScalef(width / 25.0f, height / 25.0f, 1);
    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, _box_indices);

    glPopMatrix();

#if 0
/* Cube Ball */
    for (int i = 0; i < _boxes; i++)
    {
        glPushMatrix();
        vec3 position;
        glm_vec3_copy(boxes[i]->aabb.base, position);
        glTranslatef(position[0], position[1], position[2]);
        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, indices);

        glPopMatrix();
    }
#endif

    glDisableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
#endif
}

void BOX_Create(void) {
#if 0
    for (int i = 0; i < 12; i++)
    {
        for (int j = 0; j < 12; j++)
        {
            AABB aabb = AABB_Create((vec3){2.0f + i, 8, 2.0f + j}, (vec3){1, 1, 1});
            //boxes[_boxes++] = PHYS_AddRigidbodyEx(&aabb, 1.0f, 1.0f, 0.86f, 1.0f, NULL);
            boxes[_boxes++] = PHYS_AddRigidbody(&aabb);
        }
    }
    for (int i = 0; i < 4; i++)
    {
        for (int j = 0; j < 4; j++)
        {
            //AABB aabb = AABB_Create((vec3){6 + i, 10, 6 + j}, (vec3){1, 1, 1});
            //boxes[_boxes++] = PHYS_AddRigidbodyEx(&aabb, 1.0f, 1.0f, 0.0f, 1.0f, NULL);
        }
    }
    /*for (int i = _boxes; i < BOXES; i++)
    {
        AABB aabb = AABB_Create((vec3){2 + rand() % 14, 6 + (rand() % 8), 2 + rand() % 14}, (vec3){1, 1, 1});
        boxes[_boxes++] = PHYS_AddRigidbody(&aabb);
    }*/
#endif
#if 0
/* Sphere Ball */
    int N = 140;
    float scale = 7.6f;
    float i = M_PI * (3.0f - SQRT(5));
    float o = 2.0f / N;
    for (int k = 0; k < N; k++)
    {
        float y = k * o - 1 + (o / 2.0f);
        float r = SQRT(1 - y * y);
        float phi = k * i;
        AABB aabb = AABB_Create((vec3){COS(phi) * r * scale, y * scale, SIN(phi) * r * scale}, (vec3){10, 10, 10});
        boxes[_boxes++] = PHYS_AddRigidbody(&aabb);
    }
    /*for (int i = 0; i < 5; i++)
    {
        for (int j = 0; j < 5; j++)
        {
            AABB aabb = AABB_Create((vec3){2.0f * i, j * 2.0f, 0}, (vec3){10, 10, 10});
            boxes[_boxes++] = PHYS_AddRigidbody(&aabb);
        }
    }*/
#endif
}
//static float lastUpdate = 0.0f;

void BOX_Update(void) {
#if 0
    float now = Sys_FloatTime();
    if (now - lastUpdate > 0.15f)
    {
        lastUpdate = now;
        if (INPT_Button(BTN_A))
        {
            if (_boxes < BOXES)
            {
                AABB aabb = AABB_Create((vec3){2 + rand() % 14, 6 + (rand() % 8), 2 + rand() % 14}, (vec3){1, 1, 1});
                boxes[_boxes++] = PHYS_AddRigidbody(&aabb);
            }
        }
    }
#endif
}
