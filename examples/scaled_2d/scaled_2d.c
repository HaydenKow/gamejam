/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\scaled_2d\scaled_2d.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\scaled_2d
 * Created Date: Monday, April 13th 2020, 5:03:20 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2020 HaydenKow
 */

#include "scaled_2d.h"

#include <common/input.h>
#include <common/image_loader.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("Scaled 2D | Integrity", WINDOWED);

static void Scaled_Init(void);
static void Scaled_Exit(void);
static void Scaled_Update(float time);
static void Scaled_Render2D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Scaled_Init, &Scaled_Exit, &Scaled_Render2D, NULL, &Scaled_Update, SCENE_BLOCK);
SCENE(scene_scaled_test, &Scaled_Init, NULL, &Scaled_Render2D, NULL, &Scaled_Update, SCENE_BLOCK);

static int Scaled_loaded = 0;

static tx_image *ui_texture;
static sprite circle, circle_trans;

static void Scaled_Init(void) {
  if (!Scaled_loaded) {

    /* Load Assets */
    ui_texture = IMG_load("ui_stuff.png");
    RNDR_CreateTextureFromImage(ui_texture);
    circle = IMG_create_sprite_alt(ui_texture, 2, 2, 32, 32);
    circle_trans = IMG_create_sprite_alt(ui_texture, 36, 2, 32, 32);
    UI_SetVirtualToReal();
  }
  Scaled_loaded = 1;
}

static void Scaled_Exit(void) {
  resource_object_remove(ui_texture->crc);
}

static void Scaled_Update(float time) {
  (void)time;
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }
  if (INPT_TriggerPressed(TRIGGER_L)) {
    Sys_SetFullscreen(false);
  }

  if (INPT_TriggerPressed(TRIGGER_R)) {
    Sys_SetFullscreen(true);
  }
}

static void circleSym8(int xCenter, int yCenter, int radius) {
  dimen_RECT pos = {xCenter - radius, yCenter - radius, radius * 2, radius * 2};
  UI_DrawTransSprite(&pos, 1.0f, &circle_trans);
}

static void Scaled_Render2D(float time) {
  (void)time;
#ifdef PSP
/* Under psp the demo shows proper scaled to screen */
#define V_WIDTH 480
#define V_HEIGHT 272
#else
/* everywhere else just render at half resolution */
#define V_WIDTH 320
#define V_HEIGHT 240
#endif

  /* SCR_WIDTH & SCR_HEIGHT are set to physical screen dimensions for each platform */
  /* Green Elements are new virtual resolution */
  dimen_RECT item2 = {V_WIDTH / 2, V_HEIGHT / 2, 100, 100};
  item2.x -= item2.w / 2;
  item2.y -= item2.h / 2;
  UI_SetScalingEnabled(true);
  UI_TextColor(0, 1, 0);
  DrawQuad_NoTex(&item2);
  UI_DrawStringCentered(V_WIDTH / 2, 30, "Virtual");
  UI_DrawCharacterCentered(V_WIDTH / 2, 10, 'V');
  circleSym8(V_WIDTH / 2, V_HEIGHT / 2, (V_HEIGHT / 2));

  /* Red Elements are native 640x480 resolution */
  dimen_RECT item = {640 / 2, 480 / 2, 100, 100};
  item.x -= item.w / 2;
  item.y -= item.h / 2;
  UI_SetScalingEnabled(false);
  UI_TextColor(1, 0, 0);
  DrawQuad_NoTex(&item);
  UI_DrawStringCentered(640 / 2, 420, "Reallll");
  UI_DrawCharacterCentered(620 / 2, 440, 'R');
  circleSym8(640 / 2, 480 / 2, 240);
}
