/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "spinner_scene.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("Vertex Colors | Integrity", WINDOWED);

static void Spinner_Init(void);
static void Spinner_Exit(void);
static void Spinner_Update(float time);
static void Spinner_Render2D(float time);
static void Spinner_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Spinner_Init, &Spinner_Exit, &Spinner_Render2D, &Spinner_Render3D, &Spinner_Update, SCENE_BLOCK);
SCENE(scene_spinner, &Spinner_Init, &Spinner_Exit, &Spinner_Render2D, &Spinner_Render3D, &Spinner_Update, SCENE_BLOCK);

static model_obj *spinner_obj;

float time_start;
float time_now;
static void Spinner_Init(void) {
  time_start = Sys_FloatTime();
  spinner_obj = OBJ_load("palm.obj");

  time_now = Sys_FloatTime();
}

static void Spinner_Exit(void) {
  resource_object_remove(spinner_obj->crc);
}

static void Spinner_Update(float time) {
  (void)time;
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }
}

static void Spinner_Render2D(float time) {
  (void)time;
  char msg[32];
  sprintf(msg, "Time: %8.5f", time_now - time_start);
  UI_TextSize(22);
  UI_DrawStringCentered(640 / 2, 440, msg);

  UI_TextSize(32);
  UI_DrawStringCentered(320, 20, "Press START to exit!");
}

static void Spinner_Render3D(float time) {
  glPushMatrix();

  static float counter = 0.0f;
  counter += (time * 60);

#if 0
  gluLookAt(0, 12, 30, 0, 8.0f, 0.0f, 0.0f, 1.0f, 0.0f);  // angled side from front
#else
  mat4 view;
  vec3 camera = {0, 12, 30};
  vec3 center = {0, 8.0f, 0.0f};
  glm_lookat(camera, center, GLM_YUP, view);
  glLoadMatrixf((float *)view);
#endif

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisable(GL_TEXTURE_2D);
  OBJ_bind(spinner_obj);

  //glTranslatef(0, 0, 0.0f);
  glRotatef(counter, 0, 1, 0);

  //float normalizeAmount = MIN(1 / (spinner_obj.max[0] - spinner_obj.min[0]), 1 / (spinner_obj.max[2] - spinner_obj.min[2]));
  //glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
  glDrawArrays(GL_TRIANGLES, 0, spinner_obj->num_tris);
  glEnable(GL_TEXTURE_2D);
  glPopMatrix();
}
