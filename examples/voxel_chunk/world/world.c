/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\world\world.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\world
 * Created Date: Monday, July 8th 2019, 2:39:46 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "world.h"

#include <cglm/cglm.h>

#include "renderer.h"
#include "voxel_physics/voxel_physics.h"

static chunk *_chunk;

void (*chunk_populate)(chunk *);
static chunk *chunk_map[9]; /* 0 1 2
                               3 4 5 
                               6 7 8 */
                            /* (-1, -1) (0, -1) (1, -1)
                               (-1, 0)  (0, 0)  (1, 0)
                               (-1, 1)  (0, 1)  (1, 1) */

void WRLD_Create(chunk *chnk) {
  _chunk = chnk;
  chunk_map[4] = _chunk;
  PHYS_Create(CHNK_isSolid, CHNK_isLiquid);  //, _chunk);
}

void WRLD_Draw(void) {
}

extern char dir;

void WLRD_DrawChunk(chunk *chnk) {
  if (chnk->blocks != 0 && chnk->_buffer) {
    /* Update if needed */
    CHNK_render(chnk);

    glEnableClientState(GL_COLOR_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->top[0].vert);
    glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->top[0].texture);
    glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &(chnk->top[0].color));
    glDrawArrays(GL_TRIANGLES, 0, chnk->top_index + chnk->bottom_index);

    switch (dir) {
      case 8:
      case 0:
      case 1:
        /* FRONT LEFT RIGHT */
        glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->left[0].vert);
        glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->left[0].texture);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &chnk->left[0].color);
        glDrawArrays(GL_TRIANGLES, 0, chnk->left_index + chnk->right_index + chnk->front_index);
        break;
      case 2:
      case 3:
        /* FRONT BACK RIGHT */
        glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->right[0].vert);
        glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->right[0].texture);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &chnk->right[0].color);
        glDrawArrays(GL_TRIANGLES, 0, chnk->right_index + chnk->front_index + chnk->back_index);
        break;
      case 4:
      case 5:
        /* LEFT BACK RIGHT */
        glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->left[0].vert);
        glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->left[0].texture);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &chnk->left[0].color);
        glDrawArrays(GL_TRIANGLES, 0, chnk->left_index + chnk->right_index);
        glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->back[0].vert);
        glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->back[0].texture);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &chnk->back[0].color);
        glDrawArrays(GL_TRIANGLES, 0, chnk->back_index);
        break;
      case 7:
      case 6:
        /* LEFT BACK FRONT */
        glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->left[0].vert);
        glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->left[0].texture);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &chnk->left[0].color);
        glDrawArrays(GL_TRIANGLES, 0, chnk->left_index);
        glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &chnk->front[0].vert);
        glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &chnk->front[0].texture);
        glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &chnk->front[0].color);
        glDrawArrays(GL_TRIANGLES, 0, chnk->front_index + chnk->back_index);
        break;
    }
  }
}

void WLRD_Update(float time) {
  PHYS_Tick(time);
}

extern int player_CHNK_X;
extern int player_CHNK_Z;

chunk *WLRD_GetChunkAt(int x, int y, int z) {
#if 0
  (void)y;

  int __x = x - player_CHNK_X;
  int __z = z - player_CHNK_Z;

  const int index = ((__z + 1) * 3) + __x + 1;
  if (index < 0 || index >= 64) {
    return NULL;
  }

  chunk *temp = chunk_map[index];

  if (temp == NULL) {
    chunk _temp = CHNK_create(x, z);
    chunk *ret = (chunk *)malloc(sizeof(chunk));
    memcpy(ret, &_temp, sizeof(chunk));
    (*chunk_populate)(ret);
    chunk_map[index] = ret;
    printf("Tried to find chunk [%02d, %02d, %02d] idx: %02d\n", x, y, z, index);
    return ret;
  } else {
    return temp;
  }

#if 1
  for (int i = 0; i < 9; i++) {
    if (chunk_map[x] != NULL) {
      if (chunk_map[x]->worldX == (x / 16) && chunk_map[x]->worldZ == (z / 16)) {
        return chunk_map[x];
      }
    }
  }
  (void)x;
  (void)y;
  (void)z;
  return _chunk;
#endif
#endif
  (void)x;
  (void)y;
  (void)z;
  return chunk_map[4];
}

chunk *WLRD_getChunk(void) {
  return _chunk;
}