#ifndef CHUNK_H
#define CHUNK_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world\chunk.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world
 * Created Date: Sunday, June 30th 2019, 7:58:03 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include <common.h>

#include "renderer.h"

#define CHNK_SIZE 16
#define CX (CHNK_SIZE)
#define CY (CHNK_SIZE)
#define CZ (CHNK_SIZE)

#define ATLAS_HEIGHT 256
#define ATLAS_WIDTH 256
#define ATLAS_TILES 8
#define ATLAS_TILESIZE (256 / (float)ATLAS_TILES)
#define ATLAS_PIXEL (1.0f / 256.0f)

#define ATLAS_TEX_WIDTH (ATLAS_TILESIZE / ATLAS_WIDTH)
#define ATLAS_TEX_HEIGHT (ATLAS_TILESIZE / ATLAS_HEIGHT)

#define ATLAS_X(index) ((float)(index % ATLAS_TILES) / ATLAS_TILES)
#define ATLAS_Y(index) ((float)((int)(index / ATLAS_TILES)) / ATLAS_TILES)

typedef enum {
  ABOVE,
  BELOW,
  LEFT,
  RIGHT,
  FRONT,
  BACK
} facePos;

typedef struct chunk {
  int16_t worldX;
  int16_t worldZ;
  uint8_t blk[CX][CY][CZ];
  int blocks;
  bool changed;
  glvert_fast_t *left;
  int left_index;
  glvert_fast_t *right;
  int right_index;
  glvert_fast_t *front;
  int front_index;
  glvert_fast_t *back;
  int back_index;
  glvert_fast_t *top;
  int top_index;
  glvert_fast_t *bottom;
  int bottom_index;
  glvert_fast_t *_buffer;
  int _total_elements;
} chunk;

typedef struct voxel {
  uint8_t top_index;
  uint8_t side_index;
} voxel;

chunk CHNK_create(int16_t x, int16_t z);
void CHNK_delete(chunk *chunk_temp);
void CHNK_setup(chunk *_chunk);

uint8_t CHNK_get(chunk *_chunk, int x, int y, int z);
void CHNK_set(chunk *_chunk, int x, int y, int z, uint8_t type);
bool CHNK_isblocked(chunk *_chunk, int x, int y, int z, int x2, int y2, int z2);
bool CHNK_isSolid(chunk *_chunk, int x, int y, int z);
bool CHNK_isLiquid(chunk *_chunk, int x, int y, int z);

void CHNK_addFace(chunk *_chunk, float xf, float yf, float zf, facePos facing, short size, int light_val);

void CHNK_update(chunk *_chunk);
void CHNK_render(chunk *_chunk);

#endif /* CHUNK_H */
