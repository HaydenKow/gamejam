#ifndef WORLD_H
#define WORLD_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\world\world.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\common\world
 * Created Date: Monday, July 8th 2019, 2:39:40 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include <common.h>

#include "chunk.h"

void WRLD_Create(chunk *chnk);
chunk *WLRD_getChunk(void);
chunk *WLRD_GetChunkAt(int x, int y, int z);

void WRLD_Draw(void);
void WLRD_DrawChunk(chunk *chnk);
void WLRD_Update(float time);

#endif /* WORLD_H */
