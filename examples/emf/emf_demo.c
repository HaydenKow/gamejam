/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\emf\emf_demo.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\examples\emf
 * Created Date: Tuesday, May 12th 2020, 5:15:24 pm
 * Author: HaydenKow
 *
 * Copyright (c) 2020 Hayden Kowalchuk
 */

#include "emf_demo.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

#include "gl_fast_vert.h"

WINDOW_TITLE("EMF Example", WINDOWED);

static void Spinner_Init(void);
static void Spinner_Exit(void);
static void Spinner_Update(float time);
static void Spinner_Render2D(float time);
static void Spinner_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Spinner_Init, &Spinner_Exit, &Spinner_Render2D, &Spinner_Render3D, &Spinner_Update, SCENE_BLOCK);
SCENE(scene_spinner, &Spinner_Init, &Spinner_Exit, &Spinner_Render2D, &Spinner_Render3D, &Spinner_Update, SCENE_BLOCK);

typedef struct model_emf {
  uint32_t triCount;      // Total tris in vertex segment, for now only support tris
  uint32_t modelInfo;     // bitfield, unsure, 0 bit is mesh present
  uint32_t textureID;     //
  uint32_t crc;           //
  void *vertexData;       // could be dreamcast, or psp native format depending on vertexFormat
  size_t vertexDataLen;   // Length in bytes of vertexData member
  uint8_t textureFormat;  // for included texture data: rgb, rgba, paletted?, (bit indexes) 0 = texture, 1 = vertex color,
  uint8_t vertexFormat;   // (bit indexes) 0 = dreamcast, 1 = psp
} model_emf;

static model_emf *spinner_emf;
static tx_image *spinner_tx;
static tx_image *emf_tx;

extern char error_str[64];

/* EMF DATA */

#define EMF_MESH_PRESENT (1 << 0)

#define EMF_TXR_VTXCOLOR (1 << 1)
#define EMF_TXR_TEXTURE (1 << 0)

#define EMF_VERT_DREAMCAST (1 << 0)
#define EMF_VERT_PSP (1 << 1)

#define FOURCC(a, b, c, d) ((uint32_t)(((d) << 24) | ((c) << 16) | ((b) << 8) | (a)))

#define FOURCC_EHDR FOURCC('E', 'H', 'D', 'R')
#define FOURCC_DCVT FOURCC('D', 'C', 'V', 'T')
#define FOURCC_PSVT FOURCC('P', 'S', 'V', 'T')
#define FOURCC_T24B FOURCC('T', '2', '4', 'B')
#define FOURCC_T32B FOURCC('T', '3', '2', 'B')
#define FOURCC_EEND FOURCC('E', 'E', 'N', 'D')

typedef struct EMF_CHUNK {
  uint32_t chunkType;    //FOURCC
  uint32_t chunkLength;  //Total length of data not including this header
} EMF_CHUNK;

typedef struct
{
  uint32_t triCount;       // Total tris in vertex segment, for now only support tris
  uint32_t modelInfo;      // bitfield, unsure, 0 bit is mesh present
  uint32_t textureFormat;  // for included texture data: rgb, rgba, paletted?, (bit indexes) 0 = texture, 1 = vertex color,
                           //  unsigned byte vertexFormat; //UNUSED
} EMF_HEADER;

typedef struct emf_header_chunk {
  EMF_CHUNK chunk;
  EMF_HEADER data;
} emf_header_chunk;

typedef struct emf_dcvertex_chunk {
  EMF_CHUNK chunk;
  void *data;
} emf_dcvertex_chunk;

typedef struct
{
  uint32_t channels;
  uint32_t width;
  uint32_t height;
} EMF_BITMAP;

//Similar to PNG Header, specifies EMF
static const unsigned char emfHeader[] = {0x89, 0x45, 0x4D, 0x46, 0x0D, 0x0A, 0x1A, 0x0A};

#define EMF_VERT_PREFER (EMF_VERT_DREAMCAST)
/* EMF DATA  END*/

static void loadEMF(const char *filename, model_emf *emf) {
  FILE *fp = fopen(filename, "rb");
  if (fp == NULL) {
    return;
  }

  printf("loading EMF (%s)\n", filename);

  char file_header[8];
  fread(&file_header, sizeof(emfHeader), 1, fp);

  if (memcmp(file_header, emfHeader, 4)) {
    printf("Header Error\n");
    return;
  }

  emf_header_chunk header;
  fread(&header.chunk, sizeof(EMF_CHUNK), 1, fp);

  if (header.chunk.chunkType != FOURCC_EHDR) {
    printf("Chunk Header issue!\n");
    return;
  }

  fread(&header.data, sizeof(EMF_HEADER), 1, fp);

  emf->triCount = header.data.triCount;
  emf->modelInfo = header.data.modelInfo;
  emf->textureFormat = header.data.textureFormat;

  printf("EMF [%d tris, %u, %u]\n", emf->triCount, emf->modelInfo, emf->textureFormat);

  unsigned int crc;
  fread(&crc, sizeof(unsigned int), 1, fp);

  /* Loop through all remaining chunks */
  EMF_CHUNK temp;
  int read = 0;
  while ((read = fread(&temp, sizeof(EMF_CHUNK), 1, fp)) == 1) {
    //printf("in loop: ftell: %ld\n", ftell(fp));
    switch (temp.chunkType) {
      case FOURCC_DCVT:
        printf("Found Dreamcast Verts\n");
        if (EMF_VERT_PREFER == EMF_VERT_DREAMCAST) {
          if (emf->vertexFormat == 0) {
            printf("reading %d bytes!\n", temp.chunkLength);
            emf->vertexDataLen = temp.chunkLength;
            emf->vertexData = malloc(temp.chunkLength);
            if (fread(emf->vertexData, temp.chunkLength, 1, fp) != 1) {
              printf("critical read error!\n");
            };
            emf->vertexFormat = EMF_VERT_DREAMCAST;
          } else {
            fseek(fp, temp.chunkLength, SEEK_CUR);
          }
          fread(&crc, sizeof(unsigned int), 1, fp);
          printf("DCVT crc: %08x\n", crc);
        }
        break;
      case FOURCC_PSVT:
        printf("Found PSP Verts\n");
        if (emf->vertexFormat == 0) {
          printf("reading %d bytes!\n", temp.chunkLength);
          emf->vertexDataLen = temp.chunkLength;
          emf->vertexData = malloc(temp.chunkLength);
          if (fread(emf->vertexData, temp.chunkLength, 1, fp) != 1) {
            printf("critical read error!\n");
          };
          emf->vertexFormat = EMF_VERT_PSP;
        } else {
          fseek(fp, temp.chunkLength, SEEK_CUR);
        }
        fread(&crc, sizeof(unsigned int), 1, fp);
        printf("PSVT crc: %08x\n", crc);
        break;
#if 0
      /* We can add a generic STBI image chunk */
      case FOURCC_T24B:
        printf("Found 24 Bit Texture\n");
        if (emf->textureID == 0) {
          printf("reading %d bytes!\n", temp.chunkLength);
          unsigned char *img_buffer = malloc(temp.chunkLength);
          if (fread(img_buffer, temp.chunkLength, 1, fp) != 1) {
            printf("critical read error!\n");
          };
          emf_tx = IMG_load_from_memory(img_buffer, temp.chunkLength);
          emf->textureID = RNDR_CreateTextureFromImage(emf_tx);
          free(img_buffer);
        } else {
          fseek(fp, temp.chunkLength, SEEK_CUR);
        }
        fread(&crc, sizeof(unsigned int), 1, fp);
        printf("T24B crc: %08x\n", crc);
        break;
#endif
      case FOURCC_T24B:
        printf("Found 24 Bit Texture\n");
        if (emf->textureID == 0) {
          EMF_BITMAP bitmap_header;
          if (fread(&bitmap_header, sizeof(EMF_BITMAP), 1, fp) != 1) {
            printf("critical header read error!\n");
          };
          printf("reading %d bytes!\n", sizeof(EMF_BITMAP));
          emf_tx = malloc(sizeof(tx_image));
          emf_tx->id = 0;
          emf_tx->data = malloc(temp.chunkLength - sizeof(EMF_BITMAP));
          emf_tx->channels = bitmap_header.channels;
          emf_tx->width = bitmap_header.width;
          emf_tx->height = bitmap_header.height;
          printf("tex [%dx%dx%d]\n", emf_tx->width, emf_tx->height, emf_tx->channels);

          if (fread(emf_tx->data, temp.chunkLength - sizeof(EMF_BITMAP), 1, fp) != 1) {
            printf("critical texture read error!\n");
          };
          printf("reading %d bytes!\n", temp.chunkLength - sizeof(EMF_BITMAP));

          emf->textureID = RNDR_CreateTextureFromImage(emf_tx);
        } else {
          fseek(fp, temp.chunkLength, SEEK_CUR);
        }
        fread(&crc, sizeof(unsigned int), 1, fp);
        printf("T24B crc: %08x\n", crc);
        break;
      case FOURCC_T32B:
        printf("Found 32 Bit Texture\n");
        if (emf->textureID == 0) {
          EMF_BITMAP bitmap_header;
          if (fread(&bitmap_header, sizeof(EMF_BITMAP), 1, fp) != 1) {
            printf("critical header read error!\n");
          };
          printf("reading %d bytes!\n", temp.chunkLength);
          emf_tx = malloc(sizeof(tx_image));
          emf_tx->id = 0;
          emf_tx->data = malloc(temp.chunkLength - sizeof(EMF_BITMAP));
          emf_tx->channels = bitmap_header.channels;
          emf_tx->width = bitmap_header.width;
          emf_tx->height = bitmap_header.height;
          printf("tex [%dx%dx%d]\n", emf_tx->width, emf_tx->height, emf_tx->channels);

          if (fread(emf_tx->data, temp.chunkLength - sizeof(EMF_BITMAP), 1, fp) != 1) {
            printf("critical texture read error!\n");
          };

          emf->textureID = RNDR_CreateTextureFromImage(emf_tx);
        } else {
          fseek(fp, temp.chunkLength, SEEK_CUR);
        }
        fread(&crc, sizeof(unsigned int), 1, fp);
        printf("T32B crc: %08x\n", crc);
        break;
      case FOURCC_EEND:
        fread(&crc, sizeof(unsigned int), 1, fp);
        printf("EEND crc: %08x\n", crc);
        printf("End of File!\n");
        break;
      default:
        printf("UNKNOWN CHUNK %.4s\n", (char *)&temp.chunkType);

        fread(&crc, sizeof(unsigned int), 1, fp);
        printf("UNKN crc: %08x\n", crc);

        break;
    }
  }
  fclose(fp);
}

static model_emf *EMF_load_internal(const char *path) {
  model_emf *emf = NULL;
  uint32_t crc = 0;
  int length = strlen(path);
  crc32(&crc, (uint8_t *)path, length);

  if (resource_object_find(crc) == -1) {
    emf = (model_emf *)malloc(sizeof(model_emf));
    memset(emf, 0, sizeof(model_emf));

    /* Check if file even exists */
    if (!Sys_FileExists(path)) {
      printf("ERROR: File %s not found\n", path);
      return emf;
    }

    emf->crc = crc;
    loadEMF(path, emf);
    if (emf->vertexData == NULL) {
#ifdef DEBUG
      printf("ERROR: for path(%s)\n", path);
#endif
      return emf;
    }
#ifdef DEBUG
    printf("Adding new EMF! crc: %08X, %s\n", (unsigned int)crc, path);
#endif
    resource_object_add('e', crc, emf);
    return emf;
  } else {
#ifdef DEBUG
    printf("Found already loaded EMF! crc: %08X, %s\n", (unsigned int)crc, path);
#endif
    resource_object_add('e', crc, emf);
    return (model_emf *)(resource_object_pointer(crc));
  }
}

model_emf *EMF_load(const char *path) {
  return EMF_load_internal(transform_path(path));
}

void EMF_destroy(model_emf **emf) {
  IMG_destroy(&emf_tx);
  memset((*emf)->vertexData, '\0', (*emf)->vertexDataLen);
  free((*emf)->vertexData);
  memset(*emf, '\0', sizeof(model_emf));
  free(*emf);
}

float time_start;
float time_now;
static void Spinner_Init(void) {
  resource_add_handler('e', (destroy_func)&EMF_destroy);

  time_start = Sys_FloatTime();
  spinner_emf = EMF_load("basicCharacter.emf");
  time_now = Sys_FloatTime();

  spinner_tx = IMG_load("skin_man.png");

  if (spinner_tx->data != NULL) {
    RNDR_CreateTextureFromImage(spinner_tx);
    spinner_emf->textureID = spinner_tx->id;
  }
}

static void Spinner_Exit(void) {
  /* Decrease ref count in resource manager */
  resource_object_remove(spinner_tx->crc);
  resource_object_remove(spinner_emf->crc);

  /* just free and delete */
  //EMF_destroy(spinner_emf);
  //IMG_destroy(&spinner_tx);
}

static void Spinner_Update(float time) {
  (void)time;
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }
}

static void Spinner_Render2D(float time) {
  (void)time;
  char msg[32];
  sprintf(msg, "Time: %8.5f", time_now - time_start);
  UI_TextSize(22);
  UI_DrawStringCentered(640 / 2, 440, msg);

  dimen_RECT pos = {0, 0, 256, 256};
  UI_DrawPicDimensions(&pos, spinner_tx);
  /*
  UI_TextSize(32);
  UI_DrawStringCentered(320, 440, "Press START to exit!");
  */
}

static void EMF_Draw(model_emf *emf) {
  if (emf->textureFormat == EMF_TXR_TEXTURE) {
    GL_Bind(emf_tx);
  }
  switch (emf->vertexFormat) {
    case EMF_VERT_DREAMCAST: {
      dc_fast_t *vertexData = spinner_emf->vertexData;
      glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &vertexData->vert);
      glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &vertexData->texture);
      glColorPointer(GL_BGRA, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &vertexData->color);
      glDrawArrays(GL_TRIANGLES, 0, spinner_emf->triCount);
    } break;
#if !defined(_arch_dreamcast)
    case EMF_VERT_PSP: {
      psp_fast_t *vertexData = spinner_emf->vertexData;
      /*
      glTexCoordPointer(2, GL_FLOAT, sizeof(glvert_fast_t), &vertexData->texture);
      glColorPointer(GL_BGRA, GL_UNSIGNED_BYTE, sizeof(glvert_fast_t), &vertexData->color);
      glVertexPointer(3, GL_FLOAT, sizeof(glvert_fast_t), &vertexData->vert);
      */
      glInterleavedArrays(GL_T2F_C4UB_V3F, 0, vertexData);
      glDrawArrays(GL_TRIANGLES, 0, spinner_emf->triCount);
    } break;
#endif
    default:
      assert(0);
      break;
  }
}

static void Spinner_Render3D(float time) {
  glPushMatrix();

  static float counter = 0.0f;
  counter += (time * 60);

  mat4 view;
  vec3 camera = {0, 10, 30};
  vec3 center = {0, 10.0f, 0.0f};
  //gluLookAt(0, 10, 30, 0, 10.0f, 0.0f, 0.0f, 1.0f, 0.0f);  // angled side from front
  glm_lookat(camera, center, GLM_YUP, view);
  glLoadMatrixf((float *)view);

  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  glTranslatef(0, 0, 0.0f);
  glRotatef(counter, 0, 1, 0);

  //float normalizeAmount = MIN(1 / (spinner_emf.max[0] - spinner_emf.min[0]), 1 / (spinner_emf.max[2] - spinner_emf.min[2]));
  //glScalef(normalizeAmount, normalizeAmount, normalizeAmount);
  EMF_Draw(spinner_emf);

  glPopMatrix();
}
