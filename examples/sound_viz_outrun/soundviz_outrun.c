/*
 * File: soundviz_outrun.c
 * Project: sound_viz_outrun
 * File Created: Monday, 19th April 2021 7:21:51 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <common/sound_system.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("CarViz | Integrity", WINDOWED);

static void CarViz_Init(void);
static void CarViz_Exit(void);
static void CarViz_Update(float time);
static void CarViz_Render2D(float time);
static void CarViz_Render3D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&CarViz_Init, &CarViz_Exit, &CarViz_Render2D, &CarViz_Render3D, &CarViz_Update, SCENE_BLOCK);
SCENE(scene_carviz, &CarViz_Init, &CarViz_Exit, &CarViz_Render2D, &CarViz_Render3D, &CarViz_Update, SCENE_BLOCK);

ALuint s_song;
FILE *spectrum_file = NULL;
int spectrum_frames = 0;
int spectrum_bands = 0;
float *spectrum = NULL;
const int FRAME_SYNC_OFFSET = 0;  //30;
float *spectrum_buffer = NULL;

/* Car and trees */
static model_obj *car_obj, *palm_obj, *road_obj;

#define NUM_TREES (4)
float tree_pos[5 /*NUM_TREES*/] = {25, 70, 115, 160, 205};
float tree_rot[5 /*NUM_TREES*/] = {0, 0, 0, 0, 0};

void redistribute_rgb(float *r, float *g, float *b) {
  const float threshold = 255.999f;
  float max = MAX(*r, MAX(*g, *b));
  if (max <= threshold) {
    /* no change needed */
    return;
  }
  float total = *r + *g + *b;
  if (total >= 3 * threshold) {
    /* pure white */
    *r = 255.0f;
    *g = 255.0f;
    *b = 255.0f;
  }
  float x = (3.0f * threshold - total) / (3 * max - total);
  float gray = threshold - x * max;
  /* Update values */
  *r = gray + x * (*r);
  *g = gray + x * (*g);
  *b = gray + x * (*b);
}

static float *get_spectrum_frame(int frame) {
  if (frame > spectrum_frames || frame < 0) {
    return spectrum_buffer;
  }
  /* NEW */
  return spectrum_buffer + (FRAME_SYNC_OFFSET + frame * spectrum_bands);
}

static void CarViz_Init(void) {
  car_obj = OBJ_load("supercar.obj");
  palm_obj = OBJ_load("palm.obj");
  road_obj = OBJ_load("road.obj");

  /* Lighten vertex colors */
  for (int i = 0; i < car_obj->num_tris; i++) {
    car_obj->tris[i * 8 + 5] *= 1.4f;
    car_obj->tris[i * 8 + 6] *= 1.4f;
    car_obj->tris[i * 8 + 7] *= 1.4f;
    redistribute_rgb(&car_obj->tris[i * 8 + 5], &car_obj->tris[i * 8 + 6], &car_obj->tris[i * 8 + 7]);
  }
  for (int i = 0; i < palm_obj->num_tris; i++) {
    palm_obj->tris[i * 8 + 5] *= 1.2f;
    palm_obj->tris[i * 8 + 6] *= 1.2f;
    palm_obj->tris[i * 8 + 7] *= 1.2f;
    redistribute_rgb(&palm_obj->tris[i * 8 + 5], &palm_obj->tris[i * 8 + 6], &palm_obj->tris[i * 8 + 7]);
  }
  /*
  for (int i = 0; i < road_obj->num_tris; i++) {
    road_obj->tris[i * 8 + 5] *= 1.2f;
    road_obj->tris[i * 8 + 6] *= 1.2f;
    road_obj->tris[i * 8 + 7] *= 1.2f;
    redistribute_rgb(&road_obj->tris[i * 8 + 5], &road_obj->tris[i * 8 + 6], &road_obj->tris[i * 8 + 7]);
  }
  */

  /* NEW */
  spectrum_file = fopen(transform_path("spectrum.bin"), "rb");
  fread(&spectrum_frames, sizeof(int), 1, spectrum_file);
  fread(&spectrum_bands, sizeof(int), 1, spectrum_file);

  //printf("read %d frame with %d bands\n", spectrum_frames, spectrum_bands);

  /* Setup Buffers */
  spectrum_buffer = malloc(sizeof(float) * spectrum_frames * spectrum_bands);
  spectrum = malloc(sizeof(float) * spectrum_bands);

  fread(spectrum_buffer, sizeof(float), spectrum_bands * spectrum_frames, spectrum_file);
  fclose(spectrum_file);
  spectrum_file = NULL;
  for (int i = 0; i < NUM_TREES; i++) {
    tree_rot[i] = ((float)rand() / (float)(RAND_MAX)) * 360.0f;
  }

#if !defined(_arch_dreamcast)
  create_sound(&s_song, transform_path("track_01.wav"));
  alSourcePlay(s_song);
#else
  /* Use CDDA */

  cdrom_reinit();
  int status, disk_type;
  while (status == CD_STATUS_BUSY) {
    cdrom_get_status(&status, &disk_type);
  }
  cdrom_cdda_pause();
  cdrom_cdda_play(1, 1, 0xf, CDDA_TRACKS);
  cdrom_cdda_resume();
#endif
}

static void CarViz_Exit(void) {
  resource_object_remove(car_obj->crc);
  resource_object_remove(palm_obj->crc);
  resource_object_remove(road_obj->crc);
}

static void CarViz_Update(float time) {
  (void)time;

  if (INPT_ButtonEx(BTN_A, BTN_RELEASE)) {
#if !defined(_arch_dreamcast)
    alSourceStop(s_song);
#else
    cdrom_cdda_pause();
#endif
  }

  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }

  spectrum = get_spectrum_frame(Sys_Frames());
}

static void CarViz_Render2D(float time) {
  (void)time;

  UI_TextSize(32);
  UI_DrawStringCentered(320, 460, "Get em Tiger!");
}

/* Bam Bam */
//#define spectrum_scale_factor (1 / 128.0f)
/* Goymamba */
#define spectrum_scale_factor (1 / 64.0f)

static void CarViz_Render3D(float time) {
  (void)time;
  float scrolled = (time * 16.0f);

  for (int i = 0; i < NUM_TREES; i++) {
    tree_pos[i] -= scrolled;
  }
  if (tree_pos[0] < -20) {
    /* fix position */
    float temp = tree_pos[0];
    tree_pos[0] = tree_pos[1];
    tree_pos[1] = tree_pos[2];
    tree_pos[2] = tree_pos[3];
    tree_pos[3] = tree_pos[4];
    tree_pos[4] = temp + 225.0f;
    /* save rot */
    float temp_rot = tree_rot[0];
    tree_rot[0] = tree_rot[1];
    tree_rot[1] = tree_rot[2];
    tree_rot[2] = tree_rot[3];
    tree_rot[3] = tree_rot[4];
    tree_rot[4] = temp_rot;

    /* Flip trees */
    //flip *= -1.0f;
  }
  glPushMatrix();
  glLoadIdentity();
  /* Give the camera some life */
  float x_offset = 5.0f * sinf((M_PI / 4) * Sys_FloatTime());
  float y_offset = spectrum[3] * spectrum_scale_factor * sinf((M_PI / 2) * Sys_FloatTime());
  gluLookAt(0 + x_offset, 10, 40, 0 + x_offset, 5.0f + spectrum[0] * (spectrum_scale_factor * 8) + y_offset, 0.0f, 0.0f, 1.0f, 0.0f);

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisable(GL_TEXTURE_2D);

  /* Draw a ton of trees */
  /* Tree setup stuff */
  OBJ_bind(palm_obj);
  float palm_x;
  float palm_y;
  float palm_z;
  const float palm_rot = spectrum[4] * spectrum_scale_factor * 10;
  for (int i = 0; i < NUM_TREES; i++) {
    /*
    if (i % 2 == 0) {
      palm_x = 1.5f - (spectrum[2] * spectrum_scale_factor) * flip;
      palm_y = 1.5f + (spectrum[2] * spectrum_scale_factor) * flip;
      palm_z = 1.5f - (spectrum[2] * spectrum_scale_factor) * flip;
    } else
    */
    {
      palm_x = 1.5f + (spectrum[2] * spectrum_scale_factor) /* * flip*/;
      palm_y = 1.5f - (spectrum[2] * spectrum_scale_factor) /* * flip*/;
      palm_z = 1.5f + (spectrum[2] * spectrum_scale_factor) /* * flip*/;
    }
    tree_rot[i] += palm_rot;
    /* Right Tree */
    glPushMatrix();
    glTranslatef(20, -2, -tree_pos[i]);
    glRotatef(tree_rot[i], 0, 1, 0);
    glScalef(palm_x, palm_y, palm_z);
    glDrawArrays(GL_TRIANGLES, 0, palm_obj->num_tris);
    glPopMatrix();

    /* Left Tree */
    glPushMatrix();
    glTranslatef(-20, -2, -tree_pos[i]);
    glRotatef(-tree_rot[i], 0, 1, 0);
    glScalef(palm_x, palm_y, palm_z);
    glDrawArrays(GL_TRIANGLES, 0, palm_obj->num_tris);
    glPopMatrix();
  }

  /* Draw the car */
  glPushMatrix();
  OBJ_bind(car_obj);

  const float car_x = 1.8f + (spectrum[1] * spectrum_scale_factor);
  const float car_y = 1.8f - (spectrum[3] * spectrum_scale_factor);
  const float car_z = 1.8f + (spectrum[1] * spectrum_scale_factor);

  glScalef(car_x, car_y, car_z);
  glDrawArrays(GL_TRIANGLES, 0, car_obj->num_tris);
  glPopMatrix();

  /* Draw the road */
  OBJ_bind(road_obj);
  glPushMatrix();
  glTranslatef(0, -2.5f, -44.0f * 2.0f);
  glRotatef(90.0f, 0, 1, 0);
  glScalef(4.0f, 0.8f, 0.8f);
  glDrawArrays(GL_TRIANGLES, 0, road_obj->num_tris);
  glPopMatrix();

  glPopMatrix();

  glEnable(GL_TEXTURE_2D);
}