/*
 * File: outrun_scene.h
 * Project: sound_viz_outrun
 * File Created: Monday, 19th April 2021 7:30:14 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#pragma once

#include <common.h>

extern scene scene_carviz;
