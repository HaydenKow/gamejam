{
    "entities": [
        {
            "name": "Camera",
            "position": {
                "x": 24.0,
                "y": -21.0,
                "z": 9.0
            },
            "rotation": {
                "x": 1.3962633609771729,
                "y": 0.0,
                "z": 0.7853981852531433
            },
            "scale": {
                "x": 1.0,
                "y": 1.0,
                "z": 1.0
            }
        },
        {
	        "_mesh": "cube_floor.obj",
            "_texture": "box_tex.png",
            "name": "Floor",
            "position": {
                "x": 0.0,
                "y": 0.0,
                "z": 0.0
            },
            "rotation": {
                "x": 1.5707964897155762,
                "y": -0.0,
                "z": 0.0
            },
            "scale": {
                "x": 1.0,
                "y": 1.0,
                "z": 1.0
            }
        },
        {            
            "_mesh": "willow_tex.obj",
            "_texture": "willow_tex.png",
            "_original_scale": 3.0,
            "name": "Willow_textured",
            "position": {
                "x": 6.526053428649902,
                "y": 3.026047468185425,
                "z": 0.0
            },
            "rotation": {
                "x": 1.5707964897155762,
                "y": -0.0,
                "z": -0.895882248878479
            },
            "scale": {
                "x": 2.7063825130462646,
                "y": 2.7063825130462646,
                "z": 2.7063825130462646
            }
        },
        {
            "_mesh": "willow_tex.obj",
            "_texture": "willow_tex.png",
            "_original_scale": 3.0,
            "name": "Willow_textured.001",
            "position": {
                "x": -0.6927094459533691,
                "y": -6.015636920928955,
                "z": 0.0
            },
            "rotation": {
                "x": 1.5707964897155762,
                "y": -0.0,
                "z": 2.4849331378936768
            },
            "scale": {
                "x": 2.0,
                "y": 2.0,
                "z": 2.0
            }
        },
        {
            "_mesh": "willow_tex.obj",
            "_texture": "willow_tex.png",
            "_original_scale": 3.0,
            "name": "Willow_textured.002",
            "position": {
                "x": -1.6041693687438965,
                "y": -0.9479184150695801,
                "z": 0.0
            },
            "rotation": {
                "x": 1.5707964897155762,
                "y": -0.0,
                "z": 0.0
            },
            "scale": {
                "x": 2.807523727416992,
                "y": 2.807523727416992,
                "z": 2.807523727416992
            }
        },
        {
            "_mesh": "willow_tex.obj",
            "_texture": "willow_tex.png",
            "_original_scale": 3.0,
            "name": "Willow_textured.003",
            "position": {
                "x": 1.859378457069397,
                "y": 2.187504291534424,
                "z": 0.0
            },
            "rotation": {
                "x": 1.5707964897155762,
                "y": -0.0,
                "z": -2.1111156940460205
            },
            "scale": {
                "x": 1.7944692373275757,
                "y": 1.7944692373275757,
                "z": 1.7944692373275757
            }
        }
    ]
}