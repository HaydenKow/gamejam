/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\save\vmu_dc.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\save
 * Created Date: Sunday, December 29th 2019, 7:13:02 pm
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2019 HaydenKow
 */
#include "vmu_dc.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

#include "savefile.h"
#include "scenes.h"
#include "sound.h"

static tx_image *sl_internal;

static bool _fade_in = false;
static unsigned char selected_index, last_select;
static float time_menu, time_show;
static crayon_savefile_details_t savefile_details;

extern sprite dc_controller, dc_vmu;

static void VMU_Init(void);
static void VMU_Exit(void);
static void VMU_Update(float time);
static void VMU_Render2D(float time);

/* Registers the Title scene and populates the struct */
SCENE(vmu_mgmt, &VMU_Init, &VMU_Exit, &VMU_Render2D, NULL, &VMU_Update, SCENE_BLOCK);

typedef struct minesweeper_savefile {
  int x;
  int y;
} savefile_t;

/* Our Local Data */
static savefile_t savefile;

static void scanVMU(void) {
  /* SAVEFILE MESS */
  crayon_savefile_init_savefile_details(&savefile_details, (uint8_t *)&savefile,
                                        sizeof(savefile_t), 3, 15, "Jump IT Highscores\0", "JUMPIT Scores\0",
                                        "JumpIT\0", "JUMP_IT.SYS\0");

  //pvr_init_defaults(); //Init kos

  //Load the VMU icon data
  //crayon_memory_mount_romdisk("/cd/sf_icon.img", "/Save");

  //uint8_t *vmu_lcd_icon = NULL;
  //setup_vmu_icon_load(&vmu_lcd_icon, "/Save/LCD.bin");

  //crayon_savefile_load_icon(&savefile_details, "/Save/image.bin", "/Save/palette.bin");
  //uint8_t res = crayon_savefile_load_eyecatch(&savefile_details, "Save/eyecatch3.bin"); //Must be called AFTER init

  //font_init();

  //fs_romdisk_unmount("/SaveFile");

  //Apply the VMU LCD icon (Apparently this is automatic if your savefile is an ICONDATA.VMS)
  //setup_vmu_icon_apply(vmu_lcd_icon, savefile_details.valid_vmu_screens);
  // free(vmu_lcd_icon);	//Already free-d within the above function

  //Find the first savefile (if it exists)
  for (int iter = 0; iter <= 3; iter++) {
    /* Front Slot */
    if (crayon_savefile_get_vmu_bit(savefile_details.valid_saves, iter, 1)) {  //Use the left most VMU
      savefile_details.savefile_port = iter;
      savefile_details.savefile_slot = 1;
      break;
    }

    /* Back Slot */
    if (crayon_savefile_get_vmu_bit(savefile_details.valid_saves, iter, 2)) {  //Use the left most VMU
      savefile_details.savefile_port = iter;
      savefile_details.savefile_slot = 2;
      break;
    }
  }

  printf("saves: %d", savefile_details.valid_memcards);
}

static void loadVMU(void) {
  //Try and load savefile
  crayon_savefile_load(&savefile_details);

  //No savefile yet
  if (savefile_details.valid_memcards && savefile_details.savefile_port == -1 &&
      savefile_details.savefile_slot == -1) {
    //If we don't already have a savefile, choose a VMU
    if (savefile_details.valid_memcards) {
      for (int iter = 0; iter <= 3; iter++) {
        /* Front Slot */
        if (crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, iter, 1)) {  //Use the left most VMU
          savefile_details.savefile_port = iter;
          savefile_details.savefile_slot = 1;
          break;
        }

        /* Back Slot */
        if (crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, iter, 2)) {  //Use the left most VMU
          savefile_details.savefile_port = iter;
          savefile_details.savefile_slot = 2;
          break;
        }
      }
    }
  }

  uint16_t save_res = 0;
  (void)save_res;
  if (savefile_details.valid_memcards) {
    save_res = crayon_savefile_save(&savefile_details);
    savefile_details.valid_saves = crayon_savefile_get_valid_saves(&savefile_details);
  }
}

static void VMU_Init(void) {
  sl_internal = IMG_load("sl_internal.png");
  RNDR_CreateTextureFromImage(sl_internal);
  time_menu = 0;
  time_show = 0;
  last_select = 0;

/*Note: Not working */
#if 0
    (void)&scanVMU;
    (void)&loadVMU;
#else
  scanVMU();
  loadVMU();
#endif
}

static void VMU_Exit(void) {
  resource_object_remove(sl_internal->crc);
}

static void VMU_Update(float time) {
  float now = Sys_FloatTime();
  if (now - time_menu > 0.12f) {

    dpad_t dpad = INPT_DPAD() & 15;
    unsigned char backup = selected_index;
    switch (dpad) {
      case DPAD_LEFT:
        selected_index--;
        while (!crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, selected_index % 4, (int)(selected_index / 4) + 1)) {
          if (selected_index > 7) {
            selected_index = backup;
            break;
          }
          selected_index--;
        }
        //SND_Jump();
        break;
      case DPAD_RIGHT:
        selected_index++;
        while (!crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, selected_index % 4, (int)(selected_index / 4) + 1)) {
          if (selected_index > 7) {
            selected_index = backup;
            break;
          }
          selected_index++;
        }
        //SND_Jump();
        break;
#if 1
      case DPAD_UP:
        if (selected_index > 3)
          selected_index -= 4;
        if (!crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, selected_index % 4, (int)(selected_index / 4) + 1)) {
          selected_index = backup;
        }
        //SND_Jump();
        break;
      case DPAD_DOWN:
        if (selected_index < 4)
          selected_index += 4;
        if (!crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, selected_index % 4, (int)(selected_index / 4) + 1)) {
          selected_index = backup;
        }
        //SND_Jump();
        break;
#endif
    }

    if (dpad) {
      time_menu = now;
    }
    if (selected_index > 7) {
      selected_index = backup;
    }
  }
  if (INPT_ButtonEx(BTN_A, BTN_PRESS)) {
    switch (selected_index) {
      default:
        break;
    }
  }
  if (INPT_ButtonEx(BTN_Y, BTN_PRESS)) {
    savefile_details.valid_memcards = crayon_savefile_get_valid_memcards(&savefile_details);
  }
  if (INPT_ButtonEx(BTN_B, BTN_PRESS)) {
    SCN_ChangeTo(main_title);
  }
  time_show -= time;
}

static void VMU_Render2D(float time) {
  /*
    const int header_x = 96;
    const int header_y = 48;
    const int header_i_width = 12;
    const int header_i_height = 64;
    */
  const int header_x = 128;
  const int header_y = 72;
  const int header_i_width = 12;
  const int header_i_height = 64;

  (void)time;
  static float _next = 0;
  float now = Sys_FloatTime();

  if (now > _next) {
    _next = now + 0.5f;
    _fade_in = !_fade_in;
  }

  float _alpha = (_fade_in ? (0.5f - (_next - now)) / 1.0f : ((_next - now)) / 1.0f);

  UI_TextColorEx(1, 1, 1, 1);
  UI_DrawPicDimensions(&rect_pos_fullscreen, sl_internal);

  UI_TextSize(32);
  UI_DrawStringCentered(320, 32, "Manage VMU");
  UI_TextSize(16);

  for (int i = 0; i < 4; i++) {
    UI_TextColorEx(1, 1, 1, 1);
    dimen_RECT pos = {header_x + ((6 * UI_TextSizeGet()) + header_i_width) * i, header_y, 40, 46};
    /* Headers */
    pos.x -= 6;
    pos.y -= 6;
    UI_DrawTransSprite(&pos, 1.0f, &dc_controller);
    UI_DrawCharacterCentered(header_x + (((6 * UI_TextSizeGet()) + header_i_width) * i), header_y, 'A' + i);
    UI_DrawCharacterCentered(header_x + (((6 * UI_TextSizeGet()) + header_i_width) * i), header_y, 'A' + i);

    /* VMUS */
    pos.x = (header_x + (((6 * UI_TextSizeGet()) + header_i_width) * (i))) - 18;
    pos.y = header_y * 2;
    pos.w = 64;
    pos.h = 98;
    UI_TextColorEx(1, 1, 1, 1);

    if (crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, i, 1)) {
      if (selected_index == (i % 4)) {
        UI_TextColorEx(0.5f, 1.0f, 0.5f, _alpha + 0.5f);
      }
      UI_DrawTransSprite(&pos, 1.0f, &dc_vmu);
    } else {
      //UI_TextColorEx(1.0f, 0.4f, 0.4f, 1.0f);
    }

    pos.y = header_y * 2 + pos.h + header_i_height;
    UI_TextColorEx(1, 1, 1, 1);

    if (crayon_savefile_get_vmu_bit(savefile_details.valid_memcards, i, 2)) {
      if (selected_index - 4 == i) {
        UI_TextColorEx(0.5f, 1.0f, 0.5f, _alpha + 0.5f);
      }
      UI_DrawTransSprite(&pos, 1.0f, &dc_vmu);
    } else {
      //UI_TextColorEx(1.0f, 0.4f, 0.4f, 1.0f);
    }
  }

  /* Debug */
  UI_TextColor(1, 1, 1);
  char buffer[32];
  snprintf(buffer, 32, "sel: %d", selected_index);
  UI_DrawString(12, 440, buffer);
  UI_DrawString(12, 420, "Press Y to rescan");

  /*Port*/
  UI_DrawCharacter(header_x / 2 - 12, header_y * 2, '1');
  UI_DrawCharacter(header_x / 2 - 12, header_y * 2 + 98 + header_i_height, '2');
}
