/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\null_scene.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Tuesday, November 12th 2019, 7:36:17 am
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2019 HaydenKow
 */

#ifndef SAVE_SCENE_H
#define SAVE_SCENE_H

#include <common.h>

extern scene save_scene;
void Save_Register(void);

#endif /* SAVE_SCENE_H */
