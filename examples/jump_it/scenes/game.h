/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\game.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:50:16 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef GAME_H
#define GAME_H

#include <common.h>

extern scene scene_game;
#endif /* GAME_H */
