/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\pause.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Tuesday, November 12th 2019, 7:45:11 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#ifndef PAUSE_H
#define PAUSE_H

#include <common.h>

extern scene scene_pause;

#endif /* PAUSE_H */
