/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\highscore.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Tuesday, November 12th 2019, 2:26:02 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include "highscore.h"

#include <common/renderer.h>
#include <common/input.h>
#include <common/image_loader.h>
#include <ui/ui_backend.h>

#include "scenes.h"

#include "world_2d/world.h"

static void Score_FakeData(void);
static int scores_loaded;
level_scores *score_data;
static const int SCORE_VERSION = 2;

int current_level_display;

void Highscore_Init(void);
static void Highscore_Update(float time);
static void Highscore_Render2D(float time);

/* Registers the Title scene and populates the struct */
SCENE(scene_highscore, NULL, NULL, &Highscore_Render2D, NULL, &Highscore_Update, SCENE_BLOCK);

char player_name[4];
int _index;

int new_score_index = -1;
int scores_inited = 0;

static void Score_WriteToFile(void);
static void Score_ReadFromFile(void);

void Highscore_Init(void)
{
    if (!scores_inited)
    {
        scores_loaded = 0;
        current_level_display = 0;
#ifdef _arch_dreamcast
        Score_FakeData();
#else
        if (!Sys_FileExists("scores.dat"))
        {
            Score_FakeData();
            Score_WriteToFile();
        }
        else
        {
            Score_ReadFromFile();
        }
#endif
        scores_inited = 1;
    }
    new_score_index = -1;
}
static void Score_InitialsUpdate(float time);
static void Highscore_Update(float time)
{
    (void)time;
    if (new_score_index < 0)
    {
        if (INPT_Button(BTN_A) || INPT_Button(BTN_B) || INPT_Button(BTN_START))
        {
            SCN_ChangeTo(level_select);
        }
    }
    else
    {
        Score_InitialsUpdate(time);
    }
}

static void Score_InitialsUpdate(float time)
{
    (void)time;
    static float now = 0;
    if (Sys_FloatTime() >= now + 0.1f)
    {
        //Valid: space:32, 0-9:48-57, A-Z:65-90
        if (INPT_DPADDirection(DPAD_DOWN))
        {
            player_name[_index]--;
        }
        if (INPT_DPADDirection(DPAD_UP))
        {
            player_name[_index]++;
        }
        if (player_name[_index] < (char)32) /*Space*/
        {
            player_name[_index] = 'Z';
        }
        if (player_name[_index] > (char)97) /*a*/
        {
            player_name[_index] -= 32;
        }
        if (player_name[_index] > (char)90) /*Z*/
        {
            player_name[_index] = ' ';
        }
        now = Sys_FloatTime();
    }
    if (INPT_ButtonEx(BTN_A, BTN_RELEASE))
    {
        _index++;
    }
    if (INPT_ButtonEx(BTN_B, BTN_RELEASE))
    {
        _index--;
    }
    if (_index < 0)
    {
        _index = 0;
    }
    if (_index > 2)
    {
        memcpy(Highscore_GetLevel(current_level_display)->scores[new_score_index].name, player_name, 3);
        Highscore_GetLevel(current_level_display)->scores[new_score_index].name[3] = '\0';
        new_score_index = -1;
        _index = 0;
        Score_WriteToFile();
    }
}

static void Highscore_Render2D(float time __attribute((unused)))
{
    int i;
    char line[17];

    level_scores *level_score = Highscore_GetLevel(current_level_display);

    /* Background Teal */
    UI_DrawFill(&rect_pos_fullscreen, 173, 255, 255);

    /* Draw Box */
    int height = 50;
    int width = (12 + 4) * 32;
    int thickness = 6;

    dimen_RECT pos = {320 - width / 2, 100 - height / 2, width, thickness};
    UI_DrawFill(&pos, 255.0f, 255.0f, 0); //Top
    pos.y = (100 + height / 2) - thickness;
    pos.h = thickness;
    UI_DrawFill(&pos, 255.0f, 255.0f, 0); //Bottom
    pos.y = 100 - height / 2;
    pos.w = thickness;
    pos.h = height;
    UI_DrawFill(&pos, 255.0f, 255.0f, 0); //Left
    pos.x = 320 + width / 2;
    UI_DrawFill(&pos, 255.0f, 255.0f, 0); //Right

    UI_TextSize(32);
    static bool toggle_text = 0;
    if (Sys_Frames() % 15 == 0)
    {
        toggle_text = !toggle_text;
    }
    if (toggle_text)
    {
        UI_TextColor(1.0f, 0, 0);
    }
    else
    {
        UI_TextColor(1.0f, 1.0f, 0);
    }

    /* Flashing Header */
    UI_DrawStringCentered(640 / 2, 100, "High Scores!");
    UI_TextColor(1.0f, 1.0f, 1.0f);

    UI_TextSize(24);
    int _text_size_local = UI_TextSizeGet();

    if (!level_score)
    {
        UI_DrawStringCentered(640 / 2, 200 + (32 * 0), "Error reading scores!");
        return;
    }

    for (i = 0; i < 3; i++)
    {
        memset(line, 0, sizeof(line));

        /* Checking if entering name */
        if (i == new_score_index)
        {
            snprintf(line, 16, "          %6.03f", (double)level_score->scores[i].score);
            line[16] = '\0';
            static bool flash_player = 0;
            if (Sys_Frames() % 7 == 0)
            {
                flash_player = !flash_player;
            }
            for (int j = 0; j < 3; j++)
            {
                if (j == _index)
                {
                    if (flash_player)
                        UI_DrawCharacterCentered(640 / 2 - ((7 - j) * _text_size_local) - (_text_size_local / 2), 200 + (32 * i), player_name[j]);
                }
                else
                {
                    UI_DrawCharacterCentered(640 / 2 - ((7 - j) * _text_size_local) - (_text_size_local / 2), 200 + (32 * i), player_name[j]);
                }
            }
            UI_DrawStringCentered(640 / 2, 200 + (32 * i), line);
        }
        else
        {
            snprintf(line, 16, "%s       %6.03f", level_score->scores[i].name, (double)level_score->scores[i].score);
            line[16] = '\0';
            UI_DrawStringCentered(640 / 2, 200 + (32 * i), line);
        }
    }

    if (new_score_index < 0)
        UI_DrawStringCentered(640 / 2, 400, "Any Button to Continue");

    UI_TextSize(16);
}

static void Score_FakeData(void)
{
    score highscores[3];
    /* Fake Scores */
    highscores[0] = (score){"AAA", 99.0f};
    highscores[1] = (score){"BBB", 99.0f};
    highscores[2] = (score){"CCC", 99.0f};

    /* Fake Name */
    memset(player_name, 65, 3);

    scores_loaded = 1;
    score_data = malloc(sizeof(level_scores) * scores_loaded);
    memcpy(score_data[0].scores, highscores, sizeof(score) * 3);
    score_data[0].id = 0;
    score_data[0]._future = 0;
}

static void Score_WriteToFile(void)
{

#ifdef _arch_dreamcast
    return;
#endif

    FILE *outfile;

    outfile = fopen("scores.dat", "wb");
    if (outfile == NULL)
    {
#ifdef PSP
        outfile = fopen("ms0:/scores.dat", "wb");
        if (outfile == NULL)
        {
            fprintf(stderr, "\nError open file\n");
            return;
        }
#else
        fprintf(stderr, "\nError open file\n");
        return;
#endif
    }

    /* Create our file Header */
    score_header header = (score_header){.version = SCORE_VERSION, .scores = scores_loaded};
    memcpy(header.name, player_name, 3);
    header.name[3] = '\0';

    /* Write Header */
    int wrote = fwrite(&header, sizeof(score_header), 1, outfile);

    /* write scores to file */
    wrote += fwrite(score_data, sizeof(level_scores) * scores_loaded, 1, outfile);
    
    if (wrote != 0)
    { /* Success */
    }
    else
    { /*Fail*/
    }

    fclose(outfile);
}

static void Score_ReadFromFile(void) __attribute((unused));
static void Score_ReadFromFile(void)
{
    FILE *outfile;
    outfile = fopen("scores.dat", "rb");
    if (outfile == NULL)
    {
#ifdef PSP
        outfile = fopen("ms0:/scores.dat", "wb");
        if (outfile == NULL)
        {
            fprintf(stderr, "\nError open file\n");
            return;
        }
#else
        Score_FakeData();
        Score_WriteToFile();
        return;
#endif
    }

    /* Read our file Header */
    score_header header;
    int read = fread(&header, sizeof(score_header), 1, outfile);
    header.name[3] = '\0';
    memcpy(player_name, header.name, 4);

    if (score_data)
    {
        free(score_data);
    }
    score_data = (level_scores *)malloc(sizeof(level_scores) * header.scores);
    read += fread(score_data, sizeof(level_scores) * header.scores, 1, outfile);

    for (int i = 0; i < scores_loaded; i++)
    {
        score_data[i].scores[0].name[3] = '\0';
        score_data[i].scores[1].name[3] = '\0';
        score_data[i].scores[2].name[3] = '\0';
    }

    if (read != 0)
    { /* Success */
        scores_loaded = header.scores;
    }
    else
    { /*Fail*/
    }

    fclose(outfile);
}

static int Score_FindIndex(level_scores *level_score, float time)
{
    int i;
    for (i = 0; i < 3; i++)
    {
        if (time <= level_score->scores[i].score)
        {
            return i;
        }
    }
    return -1;
}

static void Score_Swap(level_scores *level_score, int index)
{
    switch (index)
    {
    case 0:
        level_score->scores[2] = level_score->scores[1];
        level_score->scores[1] = level_score->scores[0];
        break;
    case 1:
        level_score->scores[2] = level_score->scores[1];
        break;
    default:
        break;
    }
}

static void Highscore_AddLevel(int level_id)
{
    score highscores[3];
    /* Fake Scores */
    highscores[0] = (score){"AAA", 99.0f};
    highscores[1] = (score){"BBB", 99.0f};
    highscores[2] = (score){"CCC", 99.0f};

    level_scores *temp_level = malloc(sizeof(level_scores));
    memcpy(temp_level[0].scores, highscores, sizeof(score) * 3);
    temp_level[0].id = level_id;
    temp_level[0]._future = 0;

    //Allocate 1 entry more for our new level
    level_scores *temp = (level_scores *)malloc(sizeof(level_scores) * (scores_loaded + 1));
    memcpy(temp, score_data, sizeof(level_scores) * scores_loaded);
    scores_loaded++;

    //Copy our new empty level onto the end
    memcpy(&temp[scores_loaded - 1], temp_level, sizeof(level_scores));
    
    free(temp_level);
    free(score_data);

    score_data = temp;
}

void Highscore_AddScore(int level_id, float time)
{
    level_scores *level_score = Highscore_GetLevel(level_id);
    
    if (!level_score)
    {   
        /* Level data not found, create then append */
        Highscore_AddLevel(level_id);
        level_score = Highscore_GetLevel(level_id);
    }
    new_score_index = Score_FindIndex(level_score, time);
    if (new_score_index >= 0)
    {
        Score_Swap(level_score, new_score_index);
        level_score->scores[new_score_index].score = time;
    }
    else
    {
        new_score_index = -1;
    }
}

level_scores *Highscore_GetLevel(int id)
{
    for (int i = 0; i < scores_loaded; i++)
    {
        if (score_data[i].id == id)
        {
            return &score_data[i];
        }
    }
    return NULL;
}