/*
 * File: sound.h
 * Project: jump_it
 * File Created: Monday, 19th April 2021 5:50:18 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */
#pragma once

void SND_load_all(void);
void SND_cleanup(void);
void SND_Jump(void);
void SND_Land(void);
