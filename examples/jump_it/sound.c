/*
 * File: sound.c
 * Project: jump_it
 * File Created: Monday, 19th April 2021 5:27:24 pm
 * Author: Hayden Kowalchuk
 * -----
 * Copyright (c) 2021 Hayden Kowalchuk, Hayden Kowalchuk
 * License: BSD 3-clause "New" or "Revised" License, http://www.opensource.org/licenses/BSD-3-Clause
 */

#include <common/sound_system.h>

static ALuint s_jump = 0;
static ALuint s_land = 0;

void SND_load_all(void) {
#ifdef SOUND
  create_sound(&s_jump, "jump_loud.wav");
  create_sound(&s_land, "land_loud.wav");
#endif
}

void SND_Jump(void) {
  SND_Play(s_jump);
}

void SND_Land(void) {
  SND_Play(s_land);
}

void SND_cleanup(void) {
#ifdef SOUND
  alDeleteSources(1, &s_jump);
  alDeleteSources(1, &s_land);
#endif
}