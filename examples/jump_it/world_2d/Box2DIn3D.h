#ifndef BOX2DIN3D_H
#define BOX2DIN3D_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\Box2DIn3D.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Thursday, November 14th 2019, 2:54:52 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

void BOX2D_DrawIn3D(void);
static inline void BOX2D_Update(void){}

#endif /* BOX2DIN3D_H */
