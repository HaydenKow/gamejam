/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\levels.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Sunday, November 17th 2019, 10:00:52 pm
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2019 HaydenKow
 */

#include <cglm/cglm.h>
#include <limits.h>

#include "2d_physics/2d_physics.h"
#include "input.h"
#include "player.h"
#include "renderer.h"
#include "ui/ui_backend.h"
#include "world.h"

/* Declarations here to help build Array */
static void level1(void);
static void level2(void);
static void level3(void);
static void level4(void);
static void level5(void);
static void level6(void);

/* Defines what levels we want to mark available */
//char level_available[9] = {1, 1, 1, 1, 0, 0, 0, 0, 0};
char level_available[9] = {1, 1, 1, 1, 1, 1, 1, 1, 1};

/* Defines a pointer to each other levels we will call later to create them */
/* Note: This further modifies the above array,
 * if you have a level marked available but not pointer it will be disallowed
 */
void (*level_create[9])(void) = {&level1, &level2, &level3, &level4, &level5, &level6, NULL, NULL, NULL};

int level_ids[9] = {10, 12, 1, 4, 2, 8, INT_MAX, INT_MAX, INT_MAX};

bool WRLD_2D_LevelAvailable(int id) {
  return (!!level_available[id]) && ((id <= 5) ? (level_create[id] != NULL) : 1);
}

static void vertical_platform(void *a, void *b) {
  (void)a;
  rigidbody_2d *player = (rigidbody_2d *)b;
  RBDY_2D_ApplyImpulse(player, (vec2){0, -6.0f});
}

static void level1(void) {
  /* ID == 10 */
  current_level_id = 10;

  vec2 pos;
  vec2 size = {25, 25};
  AABB_2D aabb;

  pos[0] = 320;
  pos[1] = 400;
  size[0] = 640;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  for (int i = 1; i < 6; i++) {
    pos[0] = 320;
    pos[1] = 400 - (i * 25);
    size[0] = 25;
    size[1] = 25;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 0.8f, 0.75f, 0);
  }

  pos[0] = 640;
  pos[1] = 200;
  size[0] = 100;
  size[1] = 100;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->layer = 2;
  bodies[boxes - 1]->onCollide = &win_collide;
}

static void level2_update(void) {
  static float slider_x = 400;

  static int slider_dir = 1;
  slider_x += slider_dir * 3;
  if (slider_x >= 700) {
    slider_dir = -1;
  }
  if (slider_x <= 1) {
    slider_dir = 1;
  }

  /* Platform */
  static float platform_x = 400;

  static int platform_dir = 1;
  platform_x += platform_dir;
  if (platform_x >= 400) {
    platform_dir = -1;
  }
  if (platform_x <= 100) {
    platform_dir = 1;
  }

  if (boxes_2d > 2) {
    /* Left/Right */
    AABB_2D_Translate(&bodies[1]->shape, (vec2){slider_dir, 0.0f});
    bodies[1]->friction = 1.0f;
    bodies[1]->restitution = 0.0f;

    /* Up/Down */
    AABB_2D_Translate(&bodies[3]->shape, (vec2){0.0f, platform_dir});
    bodies[1]->friction = 1.0f;
    bodies[1]->restitution = 0.0f;
  }
}

static void level2(void) {
  /* ID == 10 */
  current_level_id = 12;

  WRLD_CustomUpdate = &level2_update;

  vec2 pos;
  vec2 size = {25, 25};
  AABB_2D aabb;

  /* Left Start */
  pos[0] = 100;
  pos[1] = 400;
  size[0] = 200;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  /* Horizontal slider */
  pos[0] = 400;
  pos[1] = 400;
  size[0] = 100;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  /* Middle Thing */
  pos[0] = 700;
  pos[1] = 400;
  size[0] = 200;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  /* Platform slider */
  pos[0] = 900;
  pos[1] = 400;
  size[0] = 100;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->onCollide = &vertical_platform;

  /* Right Thing */
  pos[0] = 1100;
  pos[1] = 100;
  size[0] = 200;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  pos[0] = 1200;
  pos[1] = 0;
  size[0] = 100;
  size[1] = 100;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->layer = 2;
  bodies[boxes - 1]->onCollide = &win_collide;
}

/* classic physics example */
static void level3(void) {
  /* ID == 0 */
  current_level_id = 1;
  vec2 pos;
  vec2 size = {25, 25};
  AABB_2D aabb;
  for (int j = 0; j < 6; j++) {
    for (int i = 4; i < 11; i++) {
      pos[0] = 100 + ((i + 1) * 75);
      pos[1] = 100 + (-100 * j) - (i * 50);

      //size = (vec2){25, 25};
      aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
      bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 100, 0.8f, 0.75f, 1);  //PHYS_2D_AddRigidbody(&aabb);
    }
  }

  pos[0] = 320;
  pos[1] = 400;
  size[0] = 640;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  pos[0] = 640.1f + 320;
  pos[1] = 400;
  size[0] = 640;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  pos[0] = 1100;
  pos[1] = 200;
  size[0] = 100;
  size[1] = 100;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->layer = 2;
  bodies[boxes - 1]->onCollide = &win_collide;
}

#if 0
static void level2(void)
{
    /* ID == 1 */
    current_level_id = 1;
    vec2 pos;
    vec2 size;
    AABB_2D aabb;

    pos[0] = 320;
    pos[1] = 400;
    size[0] = 640;
    size[1] = 25;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

    pos[0] = 640.1f + 320;
    pos[1] = 400;
    size[0] = 640;
    size[1] = 25;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

    pos[0] = 500;
    pos[1] = 200;
    size[0] = 100;
    size[1] = 100;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
    bodies[boxes - 1]->layer = 2;
    bodies[boxes - 1]->onCollide = &win_collide;
}
#endif

static void level4_update(void) {
  if (boxes_2d > 2) {
    AABB_2D_Translate(&bodies[1]->shape, (vec2){0.0f, -1.0f});
    bodies[1]->friction = 1.0f;
    bodies[1]->restitution = 0.0f;
  }
}

static void level4(void) {
  /* ID == 4*/
  current_level_id = 4;
  vec2 pos;
  vec2 size;
  AABB_2D aabb;

  pos[0] = 320;
  pos[1] = 400;
  size[0] = 640;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  pos[0] = 200;
  pos[1] = 500;
  size[0] = 100;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->onCollide = &vertical_platform;

  pos[0] = 640.1f + 320;
  pos[1] = 400;
  size[0] = 640;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  pos[0] = 500;
  pos[1] = 200;
  size[0] = 100;
  size[1] = 100;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->layer = 2;
  bodies[boxes - 1]->onCollide = &win_collide;

  WRLD_CustomUpdate = &level4_update;
}

static void level5_update(void) {
  if (boxes_2d > 4) {
    AABB_2D_Translate(&bodies[2]->shape, (vec2){2.5f, 0.0f});
    bodies[2]->friction = 1.0f;
    bodies[2]->restitution = 0.0f;
  }
}

static void level5(void) {
  /* ID == 2 */
  current_level_id = 2;
  vec2 pos;
  vec2 size = {25, 25};
  AABB_2D aabb;

  float width = 800.0f;
  float y = 400.0f;
  for (int j = 0; j < 6; j++) {
    width /= 2;
    y -= 25;
    pos[0] = 320;
    pos[1] = y;
    size[0] = width;
    size[1] = 25;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  }

  width = 800.0f;
  y = 400.0f;

  for (int j = 0; j < 6; j++) {
    width /= 2;
    y -= 25;
    pos[0] = 1400;
    pos[1] = y;
    size[0] = width;
    size[1] = 25;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  }

  pos[0] = 2500;
  pos[1] = 200;
  size[0] = 100;
  size[1] = 100;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->layer = 2;
  bodies[boxes - 1]->onCollide = &win_collide;

  WRLD_CustomUpdate = &level5_update;
}

static void level6(void) {
  /* ID == 8 */
  current_level_id = 8;
  vec2 pos;
  vec2 size = {25, 25};
  AABB_2D aabb;

  /* Left Start */
  pos[0] = 100;
  pos[1] = 400;
  size[0] = 200;
  size[1] = 25;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);

  float width = 400.0f;
  float distance = 200.0f;
  float y = 400.0f;
  for (int j = 0; j < 6; j++) {
    y -= 100;
    pos[0] = 400 + (j * (distance + width));
    pos[1] = y;
    size[0] = width;
    size[1] = 25;
    aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
    bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  }

  pos[0] = 3400;
  pos[1] = y - 100;
  size[0] = 100;
  size[1] = 100;
  aabb = AABB_2D_Create((vec2){pos[0] - (size[0] / 2), pos[1] - (size[1] / 2)}, (vec2){pos[0] + (size[0] / 2), pos[1] + (size[1] / 2)});
  bodies[boxes++] = PHYS_2D_AddRigidbodyEx(&aabb, 0, 1, 1, 0);
  bodies[boxes - 1]->layer = 2;
  bodies[boxes - 1]->onCollide = &win_collide;
}