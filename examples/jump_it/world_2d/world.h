#ifndef WORLD_2D_H
#define WORLD_2D_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\world.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Monday, October 7th 2019, 10:17:32 am
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */

#include <common.h>

#define READY_TIME (0.5f)
extern float time_accum;
extern float time_clear;
extern int level_ids[];

void WRLD_2D_Create(void);
void WRLD_2D_Destroy(void);

void WRLD_2D_Draw(void);
void WLRD_2D_Update(float time);

int WORLD_2D_GetLevel(void);
int WORLD_2D_GetLevelID(void);
void WRLD_2D_SetLevel(int id);
bool WRLD_2D_LevelAvailable(int id);

#endif /* WORLD_2D_H */
