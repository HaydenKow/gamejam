#ifndef PLAYER_2D_H
#define PLAYER_2D_H
/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d\player.h
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\world_2d
 * Created Date: Wednesday, October 16th 2019, 7:40:25 pm
 * Author: Hayden Kowalchuk
 * 
 * Copyright (c) 2019 HaydenKow
 */
#include <common.h>
#include "2d_physics/2d_physics.h"

void PLYR_2D_Draw(void);
void PLYR_2D_Create(void);
void PLYR_2D_Destroy(void);
void PLYR_2D_Update(void);
rigidbody_2d *PLYR_2D_Get(void);
void PLYR_2D_ResetJumpSimple(void);
void PLYR_2D_DisableJump(void);

#endif /* PLAYER_2D_H */
