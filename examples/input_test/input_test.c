/*
 * Filename: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene\menu.c
 * Path: d:\Dev\Dreamcast\UB_SHARE\gamejam\game\src\scene
 * Created Date: Monday, November 11th 2019, 9:40:56 pm
 * Author: Hayden Kowalchuk
 *
 * Copyright (c) 2019 HaydenKow
 */

#include "input_test.h"

#include <common/image_loader.h>
#include <common/input.h>
#include <common/obj_loader.h>
#include <common/renderer.h>
#include <common/resource_manager.h>
#include <ui/ui_backend.h>

WINDOW_TITLE("input_test", WINDOWED);

static void Input_Init(void);
static void Input_Exit(void);
static void Input_Update(float time);
static void Input_Render2D(float time);

/* Registers the Title scene and populates the struct */
STARTUP_SCENE(&Input_Init, &Input_Exit, &Input_Render2D, NULL, &Input_Update, SCENE_BLOCK);
SCENE(scene_input_test, &Input_Init, NULL, &Input_Render2D, NULL, &Input_Update, SCENE_BLOCK);

static int input_loaded = 0;

static tx_image *input_sheet;
static sprite btn_a[2];
static sprite btn_b[2];
static sprite btn_x[2];
static sprite btn_y[2];
static sprite btn_start[2];

static sprite dpad_up[2];
static sprite dpad_down[2];
static sprite dpad_left[2];
static sprite dpad_right[2];

static tx_image *ui_texture;
static sprite circle, circle_trans;

static void Input_Init(void) {
  if (!input_loaded) {
    input_sheet = IMG_load("input_sheet_half.png");
    RNDR_CreateTextureFromImage(input_sheet);

    /* Buttons */
    // Dark, unlit
    btn_a[0] = IMG_create_sprite_scaled_alt(input_sheet, 220, 2, 80, 80, 0.5f);
    btn_b[0] = IMG_create_sprite_scaled_alt(input_sheet, 302, 2, 80, 80, 0.5f);
    btn_x[0] = IMG_create_sprite_scaled_alt(input_sheet, 384, 2, 80, 80, 0.5f);
    btn_y[0] = IMG_create_sprite_scaled_alt(input_sheet, 2, 84, 80, 80, 0.5f);
    btn_start[0] = IMG_create_sprite_scaled_alt(input_sheet, 248, 84, 108, 48, 0.5f);
    // Light, activated
    btn_a[1] = IMG_create_sprite_scaled_alt(input_sheet, 220, 166, 80, 80, 0.5f);
    btn_b[1] = IMG_create_sprite_scaled_alt(input_sheet, 302, 166, 80, 80, 0.5f);
    btn_x[1] = IMG_create_sprite_scaled_alt(input_sheet, 384, 166, 80, 80, 0.5f);
    btn_y[1] = IMG_create_sprite_scaled_alt(input_sheet, 2, 248, 80, 80, 0.5f);
    btn_start[1] = IMG_create_sprite_scaled_alt(input_sheet, 248, 248, 108, 48, 0.5f);

    /* Dpad/Analog emu */
    // Dark, unlit
    dpad_up[0] = IMG_create_sprite_scaled_alt(input_sheet, 358, 84, 61, 76, 0.5f);
    dpad_down[0] = IMG_create_sprite_scaled_alt(input_sheet, 157, 2, 61, 75, 0.5f);
    dpad_left[0] = IMG_create_sprite_scaled_alt(input_sheet, 2, 2, 76, 61, 0.5f);
    dpad_right[0] = IMG_create_sprite_scaled_alt(input_sheet, 80, 2, 75, 61, 0.5f);
    // Light, activated
    dpad_up[1] = IMG_create_sprite_scaled_alt(input_sheet, 421, 84, 61, 76, 0.5f);
    dpad_down[1] = IMG_create_sprite_scaled_alt(input_sheet, 157, 166, 61, 75, 0.5f);
    dpad_left[1] = IMG_create_sprite_scaled_alt(input_sheet, 2, 166, 76, 61, 0.5f);
    dpad_right[1] = IMG_create_sprite_scaled_alt(input_sheet, 80, 166, 75, 61, 0.5f);

    /* Load Assets */
    ui_texture = IMG_load("ui_stuff.png");
    RNDR_CreateTextureFromImage(ui_texture);
    circle = IMG_create_sprite_alt(ui_texture, 2, 2, 32, 32);
    circle_trans = IMG_create_sprite_alt(ui_texture, 36, 2, 32, 32);
  }
  input_loaded = 1;
}

static void Input_Exit(void) {
  resource_object_remove(input_sheet->crc);
  resource_object_remove(ui_texture->crc);
}

static void Input_Update(float time) {
  (void)time;
  if (INPT_ButtonEx(BTN_START, BTN_RELEASE)) {
    Sys_Quit();
  }
  if (INPT_TriggerPressed(TRIGGER_L)) {
    Sys_SetFullscreen(false);
  }

  if (INPT_TriggerPressed(TRIGGER_R)) {
    Sys_SetFullscreen(true);
  }

  /*if (INPT_ButtonEx(BTN_X, BTN_RELEASE))
    {
        printf("X Released!\n");
    }
    if (INPT_ButtonEx(BTN_Y, BTN_RELEASE))
    {
        printf("Y Released!\n");
    }
    if (INPT_ButtonEx(BTN_START, BTN_RELEASE))
    {
        printf("Start Released!\n");
    }*/
}

static void circleSym8(int xCenter, int yCenter, int radius) {
  dimen_RECT pos = {xCenter - radius, yCenter - radius, radius * 2, radius * 2};
  UI_DrawTransSprite(&pos, 1.0f, &circle_trans);
}

static void circleSym8_2(int xCenter, int yCenter, int radius) {
  dimen_RECT pos = {xCenter - radius, yCenter - radius, radius * 2, radius * 2};
  UI_DrawTransSprite(&pos, 1.0f, &circle_trans);
}

static void Input_Render2D(float time) {
  (void)time;

#if !defined(WIN32)
  /* Buttons */
  dimen_RECT pos = {440, 240, 40, 40};
  UI_DrawTransSprite(&pos, 1.0f, &btn_a[INPT_Button(BTN_A)]);
  pos.y = 200;
  pos.x = 400;
  UI_DrawTransSprite(&pos, 1.0f, &btn_x[INPT_Button(BTN_X)]);
  pos.x = 480;
  UI_DrawTransSprite(&pos, 1.0f, &btn_b[INPT_Button(BTN_B)]);
  pos.x = 440;
  pos.y = 160;
  UI_DrawTransSprite(&pos, 1.0f, &btn_y[INPT_Button(BTN_Y)]);
  dimen_RECT pos2 = {640 / 2 - btn_start[0].i_width, 300, btn_start[0].i_width, btn_start[0].i_height};
  UI_DrawTransSprite(&pos2, 1.0f, &btn_start[INPT_Button(BTN_START)]);
#else
  /* Buttons */
  UI_TextSize(40);
  UI_TextColor(1, 1, 1);

  UI_TextColor(0.5f, 0.5f, 0.5f);
  if (INPT_Button(BTN_A)) {
    UI_TextColor(1, 1, 1);
  }
  UI_DrawStringCentered(440, 240, STRING_A_BTN);

  UI_TextColor(0.5f, 0.5f, 0.5f);
  if (INPT_Button(BTN_X)) {
    UI_TextColor(1, 1, 1);
  }
  UI_DrawStringCentered(400, 200, STRING_X_BTN);

  UI_TextColor(0.5f, 0.5f, 0.5f);
  if (INPT_Button(BTN_B)) {
    UI_TextColor(1, 1, 1);
  }
  UI_DrawStringCentered(480, 200, STRING_B_BTN);

  UI_TextColor(0.5f, 0.5f, 0.5f);
  if (INPT_Button(BTN_Y)) {
    UI_TextColor(1, 1, 1);
  }
  UI_DrawStringCentered(440, 160, STRING_Y_BTN);

  UI_TextSize(16);
  UI_TextColorEx(1, 1, 1, 1);
  dimen_RECT pos2 = {640 / 2 - btn_start[0].i_width, 300, btn_start[0].i_width, btn_start[0].i_height};
  UI_DrawTransSprite(&pos2, 1.0f, &btn_start[INPT_Button(BTN_START)]);
#endif
  /* Dpad */
  dimen_RECT sprite_pos = {.x = 140, .y = 230, .w = dpad_left[0].i_height, .h = dpad_left[0].i_width};
  UI_DrawTransSprite(&sprite_pos, 1.0f, &dpad_down[INPT_DPADDirection(DPAD_DOWN)]);
  sprite_pos.y = 160;
  UI_DrawTransSprite(&sprite_pos, 1.0f, &dpad_up[INPT_DPADDirection(DPAD_UP)]);

  sprite_pos.w = dpad_left[0].i_width;
  sprite_pos.h = dpad_left[0].i_height;
  sprite_pos.x = 100;
  sprite_pos.y = 200;
  UI_DrawTransSprite(&sprite_pos, 1.0f, &dpad_left[INPT_DPADDirection(DPAD_LEFT)]);
  sprite_pos.x = 170;
  UI_DrawTransSprite(&sprite_pos, 1.0f, &dpad_right[INPT_DPADDirection(DPAD_RIGHT)]);

  if (INPT_TriggerPressed(TRIGGER_L)) {
    UI_DrawString(20, 72, "LTRIG");
  }
  if (INPT_TriggerPressed(TRIGGER_R)) {
    UI_DrawString(460, 72, "RTRIG");
  }

  UI_TextSize(16);
  char msg[16];
  snprintf(msg, 16, "%3d, %3d", INPT_AnalogI(AXES_X), INPT_AnalogI(AXES_Y));
  UI_DrawStringCentered(300, 220 - 64 - 16, msg);

  float _x = INPT_AnalogF(AXES_X);
  float _y = INPT_AnalogF(AXES_Y);

  float x = _x * SQRT(1 - _y * _y * 0.5f);
  float y = _y * SQRT(1 - _x * _x * 0.5f);

  snprintf(msg, 16, "%3.2f, %3.2f", (double)x, (double)y);
  UI_DrawStringCentered(300, 180 - 64 - 16, msg);

  UI_TextSize(32);
  circleSym8(300, 220, 64);
  circleSym8_2(300, 220, 64 / 8);

  /* Value Markers */
  /* Filtered */
  dimen_RECT pos3 = {300 - 2 + x * 64, 220 - 2 + y * 64, 4, 4};
  UI_DrawFill(&pos3, 0, 255, 0);

  /* Raw */
  dimen_RECT pos4 = {300 - 2 + (INPT_AnalogF(AXES_X) * 64.0f), 220 - 2 + (INPT_AnalogF(AXES_Y) * 64.0f), 4, 4};
  UI_DrawFill(&pos4, 255, 0, 0);

  UI_TextColorEx(1, 1, 1, 1);
  UI_TextSize(24);
  UI_DrawStringCentered(320, 420, "Triggers change fullscreen!");
  UI_TextSize(32);
  UI_DrawStringCentered(320, 460, "Press START to exit!");
}
